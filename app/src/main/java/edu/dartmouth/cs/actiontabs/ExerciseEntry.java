package edu.dartmouth.cs.actiontabs;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;

import java.lang.reflect.Array;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.sql.Blob;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Naofumi on 1/29/16.
 */
public class ExerciseEntry {
    private Long mId;

    private int mInputType = -1; // 0 FOR MANUAL, 1 FOR GPS, 2 FOR AUTOMATIC
    private int mActivityType = -1; // USE -1 FOR UNKNOWN ACTIVITY, OTHERWISE FOLLOW ORDER IN ACTIVITY STRING_ARRAY
    private Calendar mDateTime = Calendar.getInstance();
    private int mDuration = 0; // UNIT: SEC
    private double mDistance = 0; // UNIT: MILES
    private double mAvgPace = 0;
    private double mAvgSpeed = 0;
    private int mCalorie = 0;
    private double mClimb = 0;
    private int mHeartRate = 0;
    private String mComment = "";
    private ArrayList<LatLng> mLocationList = new ArrayList<LatLng>();


    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        this.mId = id;
    }

    public int getInputType() {
        return mInputType;
    }

    public void setInputType(int mInputType) {
        this.mInputType = mInputType;
    }

    public int getActivityType() {
        return mActivityType;
    }

    public void setActivityType(int mActivityType) {
        this.mActivityType = mActivityType;
    }

    public Calendar getDateTime() {
        return mDateTime;
    }

    public void setDateTime(Calendar mDateTime) {
        this.mDateTime = mDateTime;
    }

    public int getDuration() {
        return mDuration;
    }

    public void setDuration(int mDuration) {
        this.mDuration = mDuration;
    }

    public double getDistance() {
        return mDistance;
    }

    public void setDistance(double mDistance) {
        this.mDistance = mDistance;
    }

    public double getAvgPace() {
        return mAvgPace;
    }

    public void setAvgPace(double mAvgPace) {
        this.mAvgPace = mAvgPace;
    }

    public double getAvgSpeed() {
        return mAvgSpeed;
    }

    public void setAvgSpeed(double mAvgSpeed) {
        this.mAvgSpeed = mAvgSpeed;
    }

    public int getCalorie() {
        return mCalorie;
    }

    public void setCalorie(int mCalorie) {
        this.mCalorie = mCalorie;
    }

    public double getClimb() {
        return mClimb;
    }

    public void setClimb(double mClimb) {
        this.mClimb = mClimb;
    }

    public int getHeartRate() {
        return mHeartRate;
    }

    public void setHeartRate(int mHeartRate) {
        this.mHeartRate = mHeartRate;
    }

    public String getComment() {
        return mComment;
    }

    public void setComment(String mComment) {
        this.mComment = mComment;
    }

    public ArrayList<LatLng> getLocationList(){
        return this.mLocationList;
    }

    public void addLocation(Location location){
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        mLocationList.add(latLng);
    }

    // Convert Location ArrayList to byte array, to store in SQLite database
    public byte[] getLocationByteArray() {
        int[] intArray = new int[mLocationList.size() * 2];

        for (int i = 0; i < mLocationList.size(); i++) {
            intArray[i * 2] = (int) (mLocationList.get(i).latitude * 1E6);
            intArray[(i * 2) + 1] = (int) (mLocationList.get(i).longitude * 1E6);
        }

        ByteBuffer byteBuffer = ByteBuffer.allocate(intArray.length
                * Integer.SIZE);
        IntBuffer intBuffer = byteBuffer.asIntBuffer();
        intBuffer.put(intArray);

        return byteBuffer.array();
    }

    // Convert byte array to Location ArrayList
    public void setLocationListFromByteArray(byte[] bytePointArray) {

        ByteBuffer byteBuffer = ByteBuffer.wrap(bytePointArray);
        IntBuffer intBuffer = byteBuffer.asIntBuffer();

        int[] intArray = new int[bytePointArray.length / Integer.SIZE];
        intBuffer.get(intArray);

        int locationNum = intArray.length / 2;

        for (int i = 0; i < locationNum; i++) {
            LatLng latLng = new LatLng((double) intArray[i * 2] / 1E6F,
                    (double) intArray[i * 2 + 1] / 1E6F);
            mLocationList.add(latLng);
        }
    }

}
