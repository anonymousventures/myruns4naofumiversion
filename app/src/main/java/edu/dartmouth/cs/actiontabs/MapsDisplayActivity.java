package edu.dartmouth.cs.actiontabs;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Map;

public class MapsDisplayActivity extends FragmentActivity {

    private String TAG = "MapsDisplayActivity";

    private GoogleMap mMap;

    private ExerciseEntryDbHelper mDbHelper;
    private ExerciseEntry mEntry;
    private Float mCurSpeed = 0.0f;
    private Intent mTrackingServiceIntent; // for tracking service
    private CustomServiceConnection mServiceConnection = new CustomServiceConnection();
    /*
    *   MAP Components
    * */
    private Polyline mPolyline;
    private Marker mStartMarker;
    private Marker mEndMarker;

    /*
    *   UI Components
    * */
    private Button mSaveButton;
    private Button mCancelButton;
    private TextView mTypeText;
    private TextView mAvgSpeedText;
    private TextView mCurSpeedText;
    private TextView mClimbText;
    private TextView mCalorieText;
    private TextView mDistanceText;



    private Boolean mServiceStarted = false;
    private Boolean mBound = false;

    private TrackingService mTrackingService;
    private UpdateReceiver mUpdateReceiver;

    private int mInputType; // 1 for gps, 2 for automatic

    //private String mCurrentActivityType;
    private ArrayList<Integer> mActivityTypeArrayList;
    private Map<Integer,Integer> mFreq;

    // running, walking, standing, others
    static int[] predActivity = new int[4];




    /*
    *   LIFECYCLE OVERLOAD
    * */

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Log.d(TAG,"on create called");

//        notifyServiceReceiver = new NotifyServiceReceiver();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        setUpMapIfNeeded();
        mDbHelper = new ExerciseEntryDbHelper(this);
        mUpdateReceiver = new UpdateReceiver();
        mInputType = getIntent().getIntExtra("inputType", -1);
        if (mInputType == 1){ // FOR GPS, GET ACTIVITY_TYPE
            startService(getIntent().getIntExtra("activityType", -1));
        }else if (mInputType == 2){ // FOR AUTOMATIC, SET ACT_TYPE BE -1
            startService(-1);
        }
        if (savedInstanceState != null){
            predActivity = savedInstanceState.getIntArray("Freq");
        }



        mCancelButton = (Button) findViewById(R.id.maps_cancel_button);
        mCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopService();
                finish();
            }
        });
        mSaveButton = (Button) findViewById(R.id.maps_save_button);
        mSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mEntry != null){
                    //if it is automatic activity, correctly set the activity
                    if (mInputType == 2){
                        int max_index = 0;
                        int max_value = 0;
                        for (int i=0; i<predActivity.length; i++){
                            if (predActivity[i]>max_value){
                                max_index = i;
                                max_value = predActivity[i];
                            }
                        }
                        switch (max_index){ // corresponding values in R.activity_type_array
                            case 0: mEntry.setActivityType(2);break;
                            case 1: mEntry.setActivityType(1);break;
                            case 2: mEntry.setActivityType(0);break;
                            default: mEntry.setActivityType(13);break;
                        }

                    }


                    //add entry with asynctask
                    new InsertEntryTask(getApplicationContext()).execute(mEntry);
                    stopService();
                    finish();
                }
            }
        });

        mTypeText = (TextView) findViewById(R.id.maps_type_text);
        mAvgSpeedText = (TextView) findViewById(R.id.maps_avgspeed_text);
        mCurSpeedText = (TextView) findViewById(R.id.maps_curspeed_text);
        mClimbText = (TextView) findViewById(R.id.maps_climb_text);
        mCalorieText = (TextView) findViewById(R.id.maps_calorie_text);
        mDistanceText = (TextView) findViewById(R.id.maps_distance_text);

    } // onCreate End



    @Override
    protected void onResume() {
        Log.d(TAG, "onResume");
        super.onResume();

        // REGISTER BROADCAST RECEIVER
        IntentFilter iF = new IntentFilter(UpdateReceiver.class.getName());
        registerReceiver(mUpdateReceiver, iF);

    }

    @Override
    protected void onPause() {
        Log.d(TAG, "onPause");
        unregisterReceiver(mUpdateReceiver);
        unbindService(mServiceConnection);
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        Log.d(TAG, "onBackPressed");
        stopService();
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy");
        //stopService();
        unbindService(mServiceConnection);
        super.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putIntArray("Freq", predActivity);
    }

    /*
    *   LIFECYCLE OVERLOAD END
    * */

    /*
    *   MAP MANAGEMENT
    * */

    private void setUpMapIfNeeded(){
        Log.d(TAG, "setUpMapIfNeeded");
        if (mMap == null){
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
            if (mMap != null){
                setUpMap();
            }
        }
    }

    private void setUpMap(){
        Log.d(TAG, "setUpMap");
        mMap.addMarker(new MarkerOptions().position(new LatLng(0, 0)));
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mPolyline = mMap.addPolyline(new PolylineOptions());
    }

    private void drawTraceOnMap(){
        Log.d(TAG, "drawTraceOnMap");
        // validity check
        if (mEntry != null && mEntry.getLocationList().size()>0){

            Log.d(TAG, "inner drawtrace");
            ArrayList<LatLng> locations = mEntry.getLocationList();

            Log.d(TAG, "drawTraceOnMap here " + locations.get(locations.size()-1).latitude );

            // SET START
            if (mStartMarker == null){
                mStartMarker = mMap.addMarker(new MarkerOptions().position(locations.get(0)));
                mStartMarker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
            }

            // SET END
            if (mEndMarker == null){
                mEndMarker = mMap.addMarker(new MarkerOptions().position(locations.get(locations.size()-1)));
            }
            mEndMarker.setPosition(locations.get(locations.size()-1));

            // SET POLYLINE
            mPolyline.setPoints(locations);

            // CAMERA MOVE
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mEndMarker.getPosition(), 17.0f));
        }

    }

    /*
    *   MAP MANAGEMENT END
    * */



    private void updateStats(){
        Log.d(TAG, "updateStats");
        if (mEntry != null){
            String typeString = "Unknown";

            if (mEntry.getActivityType() != -1){
                typeString = (getResources().getStringArray(R.array.activity_type_array))[mEntry.getActivityType()];
            }

            mTypeText.setText("Type:" + typeString);
            mTypeText.setTextColor(Color.BLACK);
            mAvgSpeedText.setText("Avg speed: " + (new DecimalFormat("##.##")).format(mEntry.getAvgSpeed()) + " m/h");
            mAvgSpeedText.setTextColor(Color.BLACK);
            mCurSpeedText.setText("Cur speed: " + (new DecimalFormat("##.##")).format(mTrackingService.getCurSpeed()) + " m/h");
            mCurSpeedText.setTextColor(Color.BLACK);

            mClimbText.setText("Climb: " + (new DecimalFormat("##.##")).format(mEntry.getClimb()) + " Miles");
            mClimbText.setTextColor(Color.BLACK);
            mCalorieText.setText("Calories: " + String.valueOf(mEntry.getCalorie()));
            mCalorieText.setTextColor(Color.BLACK);
            mDistanceText.setText("Distance: " + (new DecimalFormat("##.##")).format(mEntry.getDistance()) + " Miles");
            mDistanceText.setTextColor(Color.BLACK);
        }

    }

    /*
    *   BIND HANDLING
    * */

    private void startService(int activityType){
        Log.d(TAG, "startService");
        if (!mServiceStarted){
            mTrackingServiceIntent = new Intent(this, TrackingService.class);
            mTrackingServiceIntent.putExtra("activityType", activityType);
            mTrackingServiceIntent.putExtra("inputType", mInputType);
            startService(mTrackingServiceIntent);
            bindService(mTrackingServiceIntent, mServiceConnection, BIND_AUTO_CREATE);
            mServiceStarted = true;
        }

    }

    private void stopService(){
        Log.d(TAG, "stopService");
        if (mServiceStarted){
            unbindService(mServiceConnection);
            stopService(mTrackingServiceIntent);
            mServiceStarted = false;
        }

    }

    // PASSING ServiceConnection implementation
    @Override
    public boolean bindService(Intent service, ServiceConnection conn, int flags) {
        Log.d(TAG, "bindService");
        if (! mBound){
            mBound = true;
            return super.bindService(service, conn, flags);
        }
        return false;
    }

    @Override
    public void unbindService(ServiceConnection conn) {
        Log.d(TAG, "unbindService");
        if (mBound) {
            super.unbindService(conn);
            mBound = false;
        }
    }

    /*
    *   BIND HANDLING END
    * */


    public class UpdateReceiver extends BroadcastReceiver{
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "onReceive");
            // draw
            drawTraceOnMap();
            // update text
            updateStats();

//            updateCurrentActivity();

        }
    }



    private class InsertEntryTask extends AsyncTask<ExerciseEntry, Integer, ExerciseEntry> {
        private Context mContext;
        private long databaseID;

        public InsertEntryTask(Context context) {
            mContext = context;
        }


        protected ExerciseEntry doInBackground(ExerciseEntry... entry) {
            Log.d(TAG, "doInBackground");

            ExerciseEntryDbHelper exerciseEntryDbHelper = new ExerciseEntryDbHelper(mContext);
            databaseID = exerciseEntryDbHelper.insertEntry(entry[0]);

            ExerciseEntry insertedEntry = exerciseEntryDbHelper.fetchEntryByIndex(databaseID);

            return insertedEntry;
        }

        protected void onProgressUpdate(Integer... progress) {
//        setProgressPercent(progress[0]);
        }

        protected void onPostExecute(ExerciseEntry entry) {
            Log.d(TAG, "onPostExecute");
            Toast.makeText(mContext, "Entry #"+String.valueOf(databaseID)+" saved.", Toast.LENGTH_LONG).show();

        }
    }


    private class CustomServiceConnection implements ServiceConnection{
            private IBinder mBinder;

            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                Log.d(TAG, "onServiceConnected");
                mBinder = service;
                mTrackingService = (TrackingService)((TrackingService.TrackingServiceBinder) service).getService();
                mEntry = (ExerciseEntry)((TrackingService.TrackingServiceBinder) service).getEntry();
            }

            // CALLED WHEN CRASHED
            @Override
            public void onServiceDisconnected(ComponentName name) {
                Log.d(TAG, "onServiceDisconnected");
                // need implement
                mTrackingService = null;
                //mServiceConnected = false;
            }

        public IBinder getBinder(){
            return mBinder;
        }
    }



}
