package edu.dartmouth.cs.actiontabs;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TimePicker;

import java.util.Calendar;

public class StartFragment extends Fragment implements MainActivity.FragmentInterface {

    private Spinner mInputTypeSpinner;
    private Spinner mActivityTypeSpinner;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        //inflate with startfragment xml layout
        View v = inflater.inflate(R.layout.startfragment, container, false);

        //get input spinner from xml
        mInputTypeSpinner = (Spinner) v.findViewById(R.id.input_type_spinner);

        //set inputtypespinner onclick (if needed in future)
        mInputTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                Log.d("item selected", Integer.toString(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        //add strings into spinner
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this.getActivity(),
                R.array.input_type_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mInputTypeSpinner.setAdapter(adapter);






        //get second spinner from xml
        mActivityTypeSpinner = (Spinner) v.findViewById(R.id.activity_type_spinner);

        //set onclicklistener for spinner if needed in future labs
        mActivityTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                Log.d("item selected", Integer.toString(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        //add strings to second spinner
        ArrayAdapter<CharSequence> activity_adapter = ArrayAdapter.createFromResource(this.getActivity(),
                R.array.activity_type_array, android.R.layout.simple_spinner_item);
        activity_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mActivityTypeSpinner.setAdapter(activity_adapter);






        return v;



    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


    }

    /*
    public void start(View view) {

        //create input type spinner

        //get spinner from xml
        Spinner mInputTypeSpinner =(Spinner) getActivity().findViewById(R.id.input_type_spinner);
        //get text of spinner item
        String inputType = mInputTypeSpinner.getSelectedItem().toString();


        //get activity type spinner value
        //get spinner from xml
        Spinner activityTypeSpinner =(Spinner) getActivity().findViewById(R.id.activity_type_spinner);
        //get text of spinner item
        String activityType = activityTypeSpinner.getSelectedItem().toString();


        //if spinner selection is manual
        if (inputType.equals("MANUAL")) {

            //start manualentryactivity
            Intent intent = new Intent(getActivity(), ManualEntryActivity.class);
            intent.putExtra("activityType", activityTypeSpinner.getSelectedItemPosition());
            startActivityForResult(intent, 1);
        }
        //if spinner selection is gps
        else if (inputType.equals("GPS")) {

            //start gpsentryactivity
            Intent intent = new Intent(getActivity(), MapsActivity.class);
            startActivity(intent);
        }
        //if spinner selection is automatic
        else{

            //start automatic entry activity
            Intent intent = new Intent(getActivity(), MapsActivity.class);
            startActivity(intent);
        }
    }
    */




    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

//        if (requestCode == 1) {
//            if(resultCode == Activity.RESULT_OK){
//                String result=data.getStringExtra("result");
//                long databaseID = data.getLongExtra("databaseID", 1);
//
//                ExerciseEntryDbHelper exerciseEntryDbHelper = new ExerciseEntryDbHelper(getActivity());
//                ExerciseEntry addedEntry = exerciseEntryDbHelper.fetchEntryByIndex(databaseID);
//                mAdapter.add(addedEntry);
//                mAdapter.notifyDataSetChanged();
//
//
//            }
//            if (resultCode == Activity.RESULT_CANCELED) {
//
//                // do nothing
//
//            }
//        }
//
//        if (requestCode == 2) {
//            if(resultCode == Activity.RESULT_OK){
//
//
//                System.out.println("inside this other function request code 2");
//
//                int position = data.getIntExtra("position", 1);
//
//                ExerciseEntryDbHelper exerciseEntryDbHelper = new ExerciseEntryDbHelper(this);
//                ExerciseEntry addedEntry = exerciseEntryDbHelper.fetchEntryByIndex(position);
//
//
//
//                mAdapter.remove(addedEntry);
//                mAdapter.notifyDataSetChanged();
//
//
//            }
//            if (resultCode == Activity.RESULT_CANCELED) {
//
//                // do nothing
//
//            }
//        }


    }

    public void fragmentShown(){

    }



}

