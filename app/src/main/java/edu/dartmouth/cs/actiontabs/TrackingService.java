package edu.dartmouth.cs.actiontabs;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;

import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ArffSaver;
import weka.core.converters.ConverterUtils;


public class TrackingService extends Service implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener, SensorEventListener {
    private String TAG = "TrackingService";
    private final float METER_TO_MILE = 0.000621371f;
    final static String ACTION = "NotifyServiceAction";


    private ExerciseEntry mEntry;
    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;
    private Boolean mServiceStarted = false;
    private IBinder mBinder = new TrackingServiceBinder();;
    NotificationManager mNotificationManager;

    private final int INTERVAL_VALUE = 2000;
    private final int FASTEST_INTERVAL_VALUE = 1000;

    private Location mLastLocation;
    private Float mCurSpeed=0.0f;


    //sensor service stuff
    private static final int mFeatLen = Globals.ACCELEROMETER_BLOCK_CAPACITY + 2;

    private File mFeatureFile;
    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private int mServiceTaskType;
    private String mLabel;
    private Instances mDataset;
    private Attribute mClassAttribute;
    private OnSensorChangedTask mAsyncTask;
    private boolean mStarted = false;

    private static ArrayBlockingQueue<Double> mAccBuffer;
    public static final DecimalFormat mdf = new DecimalFormat("#.##");

    //track input type
    private int mInputType;

    public TrackingService() {
    }

    public class TrackingServiceBinder extends Binder{
        // data passing by reference so those values can change in MapsDisplayActivity as well
        // => no need of getExerciseEntryFromService
        public Service getService(){
            return TrackingService.this;
        }

        public ExerciseEntry getEntry(){
            return TrackingService.this.mEntry;
        }


    }


    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate");
        mGoogleApiClient = new GoogleApiClient.Builder(this).addConnectionCallbacks(this).addOnConnectionFailedListener(this).addApi(LocationServices.API).build();
        mAccBuffer = new ArrayBlockingQueue<Double>(
                Globals.ACCELEROMETER_BUFFER_CAPACITY);

        super.onCreate();
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "onBind");
        init(intent);
        return mBinder;
    }

    public void init(Intent intent){
        Log.d(TAG, "init");
        mGoogleApiClient.connect();
        mServiceStarted = true;
        createNotification();
    }

    public void deinit() {
        Log.d(TAG, "deinit");
        mNotificationManager.cancelAll();
        mGoogleApiClient.disconnect();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand");
        if (mServiceStarted == true){
            return super.onStartCommand(intent, flags, startId);
        }
        init(intent);
        initExerciseEntry(intent.getIntExtra("inputType", -1), intent.getIntExtra("activityType", -1));

        mInputType = intent.getIntExtra("inputType", -1);

        //sensor stuff

        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        mAccelerometer = mSensorManager
                .getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);

        mSensorManager.registerListener(this, mAccelerometer,
                SensorManager.SENSOR_DELAY_FASTEST);

        Bundle extras = intent.getExtras();
        mLabel = extras.getString(Globals.CLASS_LABEL_KEY);

        mFeatureFile = new File(getExternalFilesDir(null), Globals.FEATURE_FILE_NAME);
        Log.d(Globals.TAG, mFeatureFile.getAbsolutePath());

        mServiceTaskType = Globals.SERVICE_TASK_TYPE_CLASSIFY;

        // Create the container for attributes
        ArrayList<Attribute> allAttr = new ArrayList<Attribute>();

        // Adding FFT coefficient attributes
        DecimalFormat df = new DecimalFormat("0000");

        for (int i = 0; i < Globals.ACCELEROMETER_BLOCK_CAPACITY; i++) {
            allAttr.add(new Attribute(Globals.FEAT_FFT_COEF_LABEL + df.format(i)));
        }
        // Adding the max feature
        allAttr.add(new Attribute(Globals.FEAT_MAX_LABEL));

        // Declare a nominal attribute along with its candidate values
        ArrayList<String> labelItems = new ArrayList<String>(3);
        labelItems.add(Globals.CLASS_LABEL_STANDING);
        labelItems.add(Globals.CLASS_LABEL_WALKING);
        labelItems.add(Globals.CLASS_LABEL_RUNNING);
        labelItems.add(Globals.CLASS_LABEL_OTHER);
        mClassAttribute = new Attribute(Globals.CLASS_LABEL_KEY, labelItems);
        allAttr.add(mClassAttribute);

        // Construct the dataset with the attributes specified as allAttr and
        // capacity 10000
        mDataset = new Instances(Globals.FEAT_SET_NAME, allAttr, Globals.FEATURE_SET_CAPACITY);

        // Set the last column/attribute (standing/walking/running) as the class
        // index for classification
        mDataset.setClassIndex(mDataset.numAttributes() - 1);

        mAsyncTask = new OnSensorChangedTask();
        mAsyncTask.execute();

        return super.onStartCommand(intent, flags, startId);
    }

    // setupNotification()

    private void initExerciseEntry(int inputType, int activityType){
        Log.d(TAG, "initExerciseEntry");
        mEntry = new ExerciseEntry();
        mEntry.setInputType(inputType);
        mEntry.setActivityType(activityType);

        //initialize current activity to standing because no movement initially
        //mCurrentActivityType = "Standing";
//        mActivityTypeArrayList = new ArrayList<Integer>();
        //mFreq = new HashMap<Integer, Integer>();

    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy");
        //sensor stuff

        mAsyncTask.cancel(true);
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        mSensorManager.unregisterListener(this);
        Log.i("", "");


        // cancel notifications
        deinit();
        super.onDestroy();

    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.d(TAG, "onConnected");
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(INTERVAL_VALUE);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL_VALUE);
        startLocationUpdates();

    }

    protected void startLocationUpdates() {
        Log.d(TAG, "startLocationUpdates");
        try{
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }catch(SecurityException e){
            // need permission
        }

    }


    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "onLocationChanged");
        if (mLastLocation == null){
            mLastLocation = location;
        }
        // NOTE: distanceTo returns value in meter
        mEntry.setDistance((mEntry.getDistance() + location.distanceTo(mLastLocation))*METER_TO_MILE);
        mEntry.setClimb(mEntry.getClimb() + (location.getAltitude() - mLastLocation.getAltitude()));
        mEntry.setCalorie((int) (mEntry.getDistance() / 15.0f));
        mCurSpeed = location.getSpeed();
        mEntry.setDuration((int) ((Calendar.getInstance().getTimeInMillis() - mEntry.getDateTime().getTimeInMillis()) / 1000));
        mEntry.setAvgSpeed(mEntry.getDistance() / mEntry.getDuration());

        mLastLocation = location;
        mEntry.addLocation(location);
        notifyChange();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "onConnectionSuspended");

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed");

    }

    private void notifyChange(){
        Intent intent = new Intent(MapsDisplayActivity.UpdateReceiver.class.getName());
        sendBroadcast(intent);
    }

    public Float getCurSpeed(){

        return mCurSpeed;

    }



    /*
    *   NOTIFICATION
    * */

    public void createNotification(){

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ACTION);
        Context context = getApplicationContext();
        String notificationTitle = "MyRuns4";
        String notificationText = "Tracking in progress";
        Intent myIntent = new Intent(this, MapsDisplayActivity.class);
        myIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        PendingIntent pendingIntent
                = PendingIntent.getActivity(this,
                0, myIntent,
                0);

        Notification notification = new Notification.Builder(this)
                .setContentTitle(notificationTitle)
                .setContentText(notificationText).setSmallIcon(getNotificationIcon())
                .setContentIntent(pendingIntent).build();
        mNotificationManager =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        mNotificationManager.notify(0, notification);
    }


    private int getNotificationIcon() {
        boolean useWhiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.drawable.ic_logo : R.drawable.ic_launcher;
    }

    /*
    *   NOTIFICATION END
    * */


    public double maxValue(double array[]){
        List<Double> list = new ArrayList<Double>();
        for (int i = 0; i < array.length; i++) {
            list.add(array[i]);
        }
        return Collections.max(list);
    }

    private class OnSensorChangedTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... arg0) {

            Instance inst = new DenseInstance(mFeatLen);
            inst.setDataset(mDataset);
            int blockSize = 0;
            FFT fft = new FFT(Globals.ACCELEROMETER_BLOCK_CAPACITY);
            double[] accBlock = new double[Globals.ACCELEROMETER_BLOCK_CAPACITY];
            double[] re = accBlock;
            double[] im = new double[Globals.ACCELEROMETER_BLOCK_CAPACITY];

            double max = Double.MIN_VALUE;

            while (true) {
                try {
                    // need to check if the AsyncTask is cancelled or not in the while loop
                    if (isCancelled () == true)
                    {
                        return null;
                    }

                    // Dumping buffer
                    accBlock[blockSize++] = mAccBuffer.take().doubleValue();

                    if (blockSize == Globals.ACCELEROMETER_BLOCK_CAPACITY) {
                        blockSize = 0;

                        max = maxValue(accBlock);

                        // Compute the re and im:
                        // setting values of re and im by reference.
                        fft.fft(re, im);

                        ArrayList<Double> featVect = new ArrayList<Double>();

                        for (int i = 0; i < re.length; i++) {
                            // Compute each coefficient
                            double mag = Math.sqrt(re[i] * re[i] + im[i]* im[i]);
                            // Adding the computed FFT coefficient to the
                            // featVect
                            featVect.add(Double.valueOf(mag));
                            // Clear the field
                            im[i] = .0;
                        }

                        // Finally, append max after frequency components
                        featVect.add(Double.valueOf(max));

                        Object[] array = featVect.toArray(new Object[featVect.size()]);


                        double pred = WekaClassifier.classify(array);

                                                Log.i("pred = ", pred + "" );
                        String activity = inst.classAttribute().value((int)pred);

                        MapsDisplayActivity.predActivity[(int)pred] += 1;

                        //mCurrentActivityType = activity;
                        // 0 standing, 1 walking, 2 runnning, 3 others

                        switch ((int)pred){ // corresponding values in R.activity_type_array
                            case 0: mEntry.setActivityType(2);break;
                            case 1: mEntry.setActivityType(1);break;
                            case 2: mEntry.setActivityType(0);break;
                            default: mEntry.setActivityType(13);break;
                        }

                        notifyChange();

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        protected void onCancelled() {

            Log.e("123", mDataset.size() + "");

            if (mServiceTaskType == Globals.SERVICE_TASK_TYPE_CLASSIFY) {
                super.onCancelled();
                return;
            }
            Log.i("in the loop", "still in the loop cancelled");
            String toastDisp;

            if (mFeatureFile.exists()) {

                // merge existing and delete the old dataset
                ConverterUtils.DataSource source;
                try {
                    // Create a datasource from mFeatureFile where
                    // mFeatureFile = new File(getExternalFilesDir(null),
                    // "features.arff");
                    source = new ConverterUtils.DataSource(new FileInputStream(mFeatureFile));
                    // Read the dataset set out of this datasource
                    Instances oldDataset = source.getDataSet();
                    oldDataset.setClassIndex(mDataset.numAttributes() - 1);
                    // Sanity checking if the dataset format matches.
                    if (!oldDataset.equalHeaders(mDataset)) {
                        // Log.d(Globals.TAG,
                        // oldDataset.equalHeadersMsg(mDataset));
                        throw new Exception(
                                "The two datasets have different headers:\n");
                    }

                    // Move all items over manually
                    for (int i = 0; i < mDataset.size(); i++) {
                        oldDataset.add(mDataset.get(i));
                    }

                    mDataset = oldDataset;
                    // Delete the existing old file.
                    mFeatureFile.delete();
                    Log.i("delete", "delete the file");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //toastDisp = getString(R.string.ui_sensor_service_toast_success_file_updated);

            } else {
                //toastDisp = getString(R.string.ui_sensor_service_toast_success_file_created)   ;
            }
            Log.i("save", "create saver here");
            // create new Arff file
            ArffSaver saver = new ArffSaver();
            // Set the data source of the file content
            saver.setInstances(mDataset);
            Log.e("1234", mDataset.size() + "");
            try {
                // Set the destination of the file.
                // mFeatureFile = new File(getExternalFilesDir(null),
                // "features.arff");
                saver.setFile(mFeatureFile);
                // Write into the file
                saver.writeBatch();
                Log.i("batch", "write batch here");
                //Toast.makeText(getApplicationContext(), toastDisp, Toast.LENGTH_SHORT).show();
            } catch (IOException e) {
                //toastDisp = getString(R.string.ui_sensor_service_toast_error_file_saving_failed);
                e.printStackTrace();
            }

            Log.i("toast", "toast here");
            super.onCancelled();
        }

    }


    public void onSensorChanged(SensorEvent event) {

        //only do if input type was automatic

        if (mInputType == 2) {

//            Log.d(TAG, "input type correct");


            if (event.sensor.getType() == Sensor.TYPE_LINEAR_ACCELERATION) {

                double m = Math.sqrt(event.values[0] * event.values[0]
                        + event.values[1] * event.values[1] + event.values[2]
                        * event.values[2]);

                // Inserts the specified element into this queue if it is possible
                // to do so immediately without violating capacity restrictions,
                // returning true upon success and throwing an IllegalStateException
                // if no space is currently available. When using a
                // capacity-restricted queue, it is generally preferable to use
                // offer.

                try {
                    mAccBuffer.add(new Double(m));
                } catch (IllegalStateException e) {

                    // Exception happens when reach the capacity.
                    // Doubling the buffer. ListBlockingQueue has no such issue,
                    // But generally has worse performance
                    ArrayBlockingQueue<Double> newBuf = new ArrayBlockingQueue<Double>(
                            mAccBuffer.size() * 2);

                    mAccBuffer.drainTo(newBuf);
                    mAccBuffer = newBuf;
                    mAccBuffer.add(new Double(m));
                }
            }

        }
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }



}