package edu.dartmouth.cs.actiontabs;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by gary on 1/30/16.
 */
public class EntryArrayAdapter extends ArrayAdapter<ExerciseEntry> {
    public static final String[] activityArray = {"Running", "Walking", "Standing","Cycling", "Hiking", "Downhill Skiing", "Cross-Country Skiing", "Snowboarding", "Skating", "Swimming", "Mountain Biking", "Wheelchair", "Elliptical", "Other"};
    public static final Double MILES_TO_KM = 1.60934;
    private TextView mTopText;
    private TextView mBottomText;
    private ArrayList<ExerciseEntry> entries = new ArrayList<ExerciseEntry>();

    public static class ViewHolder {
        public TextView topTextView;
        public TextView bottomTextView;
        public long databaseId;
        public int inputType;
    }

    public EntryArrayAdapter(Context context, int textViewResourceId, ArrayList<ExerciseEntry> items) {
        super(context, textViewResourceId, items);
        this.entries = items;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder = new ViewHolder();

        if (convertView == null) {
            convertView = LayoutInflater.from(this.getContext())
                    .inflate(R.layout.entry_view, parent, false);

            viewHolder.topTextView = (TextView) convertView.findViewById(R.id.entry_text_top);
            viewHolder.bottomTextView = (TextView) convertView.findViewById(R.id.entry_text_bottom);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        ExerciseEntry item = getItem(position);
        if (item!= null) {
            // My layout has only one TextView
            // do whatever you want with your string and long

            String inputType;
            switch (item.getInputType()){
                case 0: inputType = "Manual Entry"; break;
                case 1: inputType = "GPS"; break;
                case 2: inputType = "Automatic"; break;
                default: inputType = ""; break;
            }

            String activityType;
            switch (item.getActivityType()){
                case -1: activityType = "Unknown"; break;
                default: activityType = activityArray[item.getActivityType()];
            }

            Calendar time = item.getDateTime();
            String timeString = time.get(Calendar.HOUR)+":"
                    +time.get(Calendar.MINUTE)+":"
                    +time.get(Calendar.SECOND)+" "
                    +time.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.getDefault())+" "
                    +time.get(Calendar.DAY_OF_MONTH)+" "
                    +time.get(Calendar.YEAR);

            String duration = "";
            int hr=0;
            int min=0;
            if (item.getDuration() >= 60*60){
                hr = item.getDuration()/(60*60);
                duration += String.valueOf(hr) + " hrs ";
            }
            if (item.getDuration() >= 60){
                min = (item.getDuration() - hr*(60*60))/60;
                duration += String.valueOf(min) + " mins ";
            }
            duration += String.valueOf(item.getDuration() - hr*(60*60) - min*60) + " secs";

            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
            String unitpref = prefs.getString("preference_unit", "0");
            String distanceString = "";
            if (unitpref.equals("0")){
                distanceString = (new DecimalFormat("##.##")).format(item.getDistance() * MILES_TO_KM) + " Kilometers";

            }else{
                distanceString = (new DecimalFormat("##.##")).format(item.getDistance()) + " Kilometers";
            }

            viewHolder.topTextView.setText(inputType + ": " + activityType + " " + timeString);
            viewHolder.bottomTextView.setText(distanceString + ", " + duration);
            viewHolder.databaseId = item.getId();
            viewHolder.inputType = item.getInputType();

        }

        return convertView;
    }

    @Override
    public ExerciseEntry getItem(int position) {
        return entries.get(position);
    }

    public void remove(int position) {
        entries.remove(position);
    }



    public void setEntries(ArrayList<ExerciseEntry> data){
        entries.clear();
        entries.addAll(data);
        notifyDataSetChanged();
    }



}