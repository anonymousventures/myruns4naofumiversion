package edu.dartmouth.cs.actiontabs;

import android.app.Activity;
import android.app.Fragment;
import android.app.LoaderManager;
import android.content.AsyncTaskLoader;
import android.content.Context;
import android.content.Loader;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class HistoryFragment extends Fragment implements MainActivity.FragmentInterface, LoaderManager.LoaderCallbacks<ArrayList<ExerciseEntry>> {

    private ArrayList<ExerciseEntry> mEntries;
    private EntryArrayAdapter mAdapter;
    private final String[] activityArray = {"Running", "Walking", "Standing","Cycling", "Hiking", "Downhill Skiing", "Cross-Country Skiing", "Snowboarding", "Skating", "Swimming", "Mountain Biking", "Wheelchair", "Elliptical", "Other"};


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View currentView = inflater.inflate(R.layout.historyfragment, container, false);
        //get list from xml declaration
        ListView historyList = (ListView) currentView.findViewById(R.id.history_list);
        //values in the listview of manual entry activity
        String[] values = new String[] { "Date",
                "Time",
                "Duration",
                "Distance",
                "Calories",
                "Heart Rate",
                "Comment",
        };

        ExerciseEntryDbHelper exerciseEntryDbHelper = new ExerciseEntryDbHelper(getActivity());
        mAdapter = new EntryArrayAdapter(getActivity(),
                android.R.layout.simple_list_item_1, new ArrayList<ExerciseEntry>());
        historyList.setAdapter(mAdapter);
        getLoaderManager().initLoader(1, null, this).forceLoad();

        historyList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                EntryArrayAdapter.ViewHolder viewHolder = (EntryArrayAdapter.ViewHolder) view.getTag();

                System.out.println(" here it is type " + viewHolder.inputType);

                //for manual entry
                if (viewHolder.inputType == 0) {
                    long databaseId = viewHolder.databaseId;
                    Intent i = new Intent(getActivity(), EntryDetailActivity.class);
                    i.putExtra("position", databaseId);
                    startActivityForResult(i, 2);
                }
                //for gps entry
                else if (viewHolder.inputType == 1){
                    long databaseId = viewHolder.databaseId;
                    Intent i = new Intent(getActivity(), MapDetailActivity.class);
                    i.putExtra("position", databaseId);
                    startActivityForResult(i, 3);
                }


            }
        });

        return currentView;
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //getLoaderManager().initLoader(1, null, this).forceLoad();
        if (requestCode == 2) {
            if (resultCode == Activity.RESULT_OK) {
                int pos = data.getIntExtra("databaseID", 0);
                mAdapter.remove(pos);
                mAdapter.notifyDataSetChanged();
            }
        }
        else if (requestCode == 3){
            int pos = data.getIntExtra("databaseID", 0);
            mAdapter.remove(pos);
            mAdapter.notifyDataSetChanged();

        }

    }

    @Override
    public void onResume() {
        super.onResume();
        mAdapter.notifyDataSetChanged();
        //Log.d("here", "onResume called");

    }

    @Override
    public void fragmentShown(){
        getLoaderManager().initLoader(1, null, this).forceLoad();

    }

    @Override
    public Loader<ArrayList<ExerciseEntry>> onCreateLoader(int i, Bundle bundle) {
        return new ExerciseEntryLoader(getActivity());
    }

    @Override
    public void onLoadFinished(Loader<ArrayList<ExerciseEntry>> listLoader, ArrayList<ExerciseEntry> exerciseEntries) {

        mAdapter.setEntries(exerciseEntries);

    }

    @Override
    public void onLoaderReset(Loader<ArrayList<ExerciseEntry>> listLoader) {
        mAdapter.setEntries(new ArrayList<ExerciseEntry>());
    }


}

