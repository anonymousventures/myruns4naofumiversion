package edu.dartmouth.cs.actiontabs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TimePicker;


import android.preference.PreferenceManager;

import java.util.Calendar;

/**
 * Created by gary on 1/15/16.
 */
public class ManualEntryActivity extends FragmentActivity {

    private ListView mListView;


    private Long id;

    private int mInputType;        // Manual, GPS or automatic
    private int mActivityType;     // Running, cycling etc.
    private Calendar mDateTime;    // When does this entry happen
    private int mDuration;         // Exercise duration in seconds
    private double mDistance;      // Distance traveled. Either in meters or feet.
    private double mAvgPace = -1;       // Average pace
    private double mAvgSpeed = -1;      // Average speed
    private int mCalorie = -1;          // Calories burnt
    private double mClimb = -1;         // Climb. Either in meters or feet.
    private int mHeartRate = -1;        // Heart rate
    private String mComment = "nothing";       // Comments
//    private ArrayList<LatLng> mLocationList; // Location list

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_activity);

        //get list from xml declaration
        mListView = (ListView) findViewById(R.id.manual_entry_list);

       //values in the listview of manual entry activity
        String[] values = new String[] { "Date",
                "Time",
                "Duration",
                "Distance",
                "Calories",
                "Heart Rate",
                "Comment",
        };

        //mInputType
        mInputType = 0;

        //mActivityType
        Intent intent = getIntent();
        mActivityType = intent.getIntExtra("activityType", 0);

        //set calendar date to now
        mDateTime = Calendar.getInstance();

        //set miles and duration to default values
        mDistance = 0;
        mDuration = 0;





        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, values);


        // Assign adapter to ListView
        mListView.setAdapter(adapter);

        //onclick listener for listview
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {

                //get name of selected item
                String selectedItem = (mListView.getItemAtPosition(position)).toString();

                Log.d("item clicked", selectedItem);


                if (selectedItem.equals("Date")){
                    DialogFragment newFragment = new DatePickerFragment();
                    newFragment.show(getSupportFragmentManager(), "datePicker");
                }

                else if (selectedItem.equals("Time")){

                    DialogFragment newFragment = new TimePickerFragment();
                    newFragment.show(getSupportFragmentManager(), "timePicker");

                }
                else if (selectedItem.equals("Duration")){

                showDialogEntry("Duration (minutes):", "number");

                }
                else if (selectedItem.equals("Distance")){
                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    String unitpref = prefs.getString("preference_unit", "0");
                    if (unitpref.equals("0")){ // KMs
                        showDialogEntry("Distance (Kilometers):", "number");
                    }else{ // MILES
                        showDialogEntry("Distance (Miles):", "number");
                    }




                }
                else if (selectedItem.equals("Calories")){

                    showDialogEntry("Calories:", "number");

                }
                else if (selectedItem.equals("Heart Rate")){

                    showDialogEntry("Average Heart Rate (BPM):", "number");

                }
                else if (selectedItem.equals("Comment")){

                    showDialogEntry("Comment:", "text");

                }




            }
        });


    }

    //save button onclick listener
    public void save(View view) {



        //calendar already set (set mDateTime in onDateSet listener)

        //all other variables set except for location list

        //TODO: LOCATIONLIST


        ExerciseEntry entry = new ExerciseEntry();
        //these values are guaranteed to exist because they are set in onCreate
        entry.setInputType(mInputType);
        entry.setActivityType(mActivityType);
        entry.setDateTime(mDateTime);
        entry.setDistance(mDistance);
        entry.setDuration(mDuration);

        System.out.println(" distance top " + String.valueOf(mDistance));


        //other values that might not have been updated

        //TODO: remove guard statements

        Log.d("save", "save selected");

        if (mAvgPace != -1){
            entry.setAvgPace(mAvgPace);
        }
        if (mAvgSpeed != -1){
            entry.setAvgSpeed(mAvgSpeed);
        }
        if (mCalorie != -1){
            entry.setCalorie(mCalorie);
        }
        if (mClimb != -1){
            entry.setClimb(mClimb);
        }
        if (mHeartRate != -1){
            entry.setHeartRate(mHeartRate);
        }
        if (! mComment.equals("nothing")){
            entry.setComment(mComment);
        }


        Log.d("save", "past breakpoint");

//        ExerciseEntryDbHelper exerciseEntryDbHelper = new ExerciseEntryDbHelper(this);
//        long databaseID = exerciseEntryDbHelper.insertEntry(entry);

        new InsertEntryTask(getApplicationContext()).execute(entry);




//        MainActivity parentActivity = getActivi






//        public class ExerciseEntry {
//            private Long id;
//
//            private int mInputType;        // Manual, GPS or automatic
//            private int mActivityType;     // Running, cycling etc.
//            private Calendar mDateTime;    // When does this entry happen
//            private int mDuration;         // Exercise duration in seconds
//            private double mDistance;      // Distance traveled. Either in meters or feet.
//            private double mAvgPace;       // Average pace
//            private double mAvgSpeed;      // Average speed
//            private int mCalorie;          // Calories burnt
//            private double mClimb;         // Climb. Either in meters or feet.
//            private int mHeartRate;        // Heart rate
//            private String mComment;       // Comments
//            private ArrayList<LatLng> mLocationList; // Location list
//        }


//        Intent returnIntent = new Intent();
//        returnIntent.putExtra("databaseID",databaseID);
//        setResult(Activity.RESULT_OK,returnIntent);
//
//
//        finish();
    }


    private class InsertEntryTask extends AsyncTask<ExerciseEntry, Integer, ExerciseEntry> {

        private Context mContext;

        public InsertEntryTask(Context context) {
            mContext = context;
        }


        protected ExerciseEntry doInBackground(ExerciseEntry... entry) {

            ExerciseEntryDbHelper exerciseEntryDbHelper = new ExerciseEntryDbHelper(mContext);
            long databaseID = exerciseEntryDbHelper.insertEntry(entry[0]);

            ExerciseEntry insertedEntry = exerciseEntryDbHelper.fetchEntryByIndex(databaseID);

            System.out.println("doinbackground here");

            return insertedEntry;
        }

        protected void onProgressUpdate(Integer... progress) {
//        setProgressPercent(progress[0]);
        }

        protected void onPostExecute(ExerciseEntry entry) {

            System.out.println("onPostExecute done");

            postInsertion(entry);

        }
    }

    public void postInsertion (ExerciseEntry entry) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("databaseID",entry.getId());
        setResult(Activity.RESULT_OK, returnIntent);

//        mAdapter.add(entry);
//        mAdapter.notifyDataSetChanged();

        finish();


    }




    //cancel butotn onclick listener
    public void cancel(View view) {

        finish();
    }

    //internal class for timepicker dialogfragment
    public static class TimePickerFragment extends DialogFragment
            implements TimePickerDialog.OnTimeSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current time as the default values for the picker
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }

        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            // Do something with the time chosen by the user
            ManualEntryActivity parentActivity = (ManualEntryActivity) getActivity();
            parentActivity.mDateTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
            parentActivity.mDateTime.set(Calendar.MINUTE, minute);


        }
    }

    //internal class for datepicker dialogfragment
    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {

            //set mDateTime
            ManualEntryActivity parentActivity = (ManualEntryActivity) getActivity();
            parentActivity.mDateTime.set(Calendar.DAY_OF_MONTH, day);
            parentActivity.mDateTime.set(Calendar.MONTH, month);
            parentActivity.mDateTime.set(Calendar.YEAR, year);

            // Do something with the date chosen by the user
        }
    }


    //show the dialog entry for all other items in the listview besides timepicker and datepicker
    void showDialogEntry(String title, String dialogType) {

        FragmentManager fm = getSupportFragmentManager();
        MyAlertDialogFragment alertDialog = MyAlertDialogFragment.newInstance(title, dialogType);
        alertDialog.show(fm, "fragment_alert");

    }


    //internal factory class for production of dialog fragments
    public static class MyAlertDialogFragment extends DialogFragment {
        private static Double KM_TO_MILE = 0.621371;

        //factory method to create dialogfragment based on title and the dialogType
        // i.e. (does the keyboard show numbers only or display the text keyboard?)
        public static MyAlertDialogFragment newInstance(String title, String dialogType) {
            MyAlertDialogFragment frag = new MyAlertDialogFragment();
            Bundle args = new Bundle();
            args.putString("title", title);
            args.putString("dialogType", dialogType);
            frag.setArguments(args);
            return frag;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final String title = getArguments().getString("title");
            String dialogType = getArguments().getString("dialogType");
            View dialoglayout;

            LayoutInflater inflater = getActivity().getLayoutInflater();

            final EditText dialogEditText;

            //create dialog that is for inputting numbers
            if (dialogType.equals("number")) {
                dialoglayout = inflater.inflate(R.layout.manual_entry_dialog_number, null);
                dialogEditText = (EditText) dialoglayout.findViewById(R.id.dialog_number);
            }
            //create dialog that is for inputting text
            else{
                dialoglayout = inflater.inflate(R.layout.manual_entry_dialog_text, null);

                dialogEditText = (EditText) dialoglayout.findViewById(R.id.dialog_text);
            }


            //build the dialog
            return new AlertDialog.Builder(getActivity())
                    .setTitle(title)
                    .setView(dialoglayout)
                    .setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int whichButton) {
//                                    ((FragmentDialogAlarmActivity) getActivity())
//                                            .doPositiveClick();
                                    ManualEntryActivity parentActivity = (ManualEntryActivity) getActivity();

                                    System.out.println("entered onclick for dialog interface");


                                    if (title.equals("Duration (minutes):")) {

                                        parentActivity.mDuration = Integer.parseInt(dialogEditText.getText().toString());

                                    } else if (title.equals("Distance (Miles):")) {
                                        parentActivity.mDistance = Double.parseDouble(dialogEditText.getText().toString());
                                    } else if (title.equals("Distance (Kilometers):")) {
                                        parentActivity.mDistance = Double.parseDouble(dialogEditText.getText().toString());
                                        parentActivity.mDistance = KM_TO_MILE * parentActivity.mDistance;
                                    } else if (title.equals("Calories")) {

                                        parentActivity.mCalorie = Integer.parseInt(dialogEditText.getText().toString());


                                    } else if (title.equals("Average Heart Rate (BPM):")) {

                                        parentActivity.mHeartRate = Integer.parseInt(dialogEditText.getText().toString());


                                    } else if (title.equals("Comment")) {

                                        parentActivity.mComment = dialogEditText.getText().toString();


                                    }


                                }
                            })
                    .setNegativeButton("CANCEL",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int whichButton) {
//                                    ((FragmentDialogAlarmActivity) getActivity())
//                                            .doNegativeClick();
                                }
                            }).create();
        }
    }





}
