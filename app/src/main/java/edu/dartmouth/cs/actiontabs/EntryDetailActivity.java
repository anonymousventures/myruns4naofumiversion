package edu.dartmouth.cs.actiontabs;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by gary on 1/30/16.
 */
public class EntryDetailActivity extends AppCompatActivity {
    //private final String[] activityArray = {"Running", "Walking", "Standing","Cycling", "Hiking", "Downhill Skiing", "Cross-Country Skiing", "Snowboarding", "Skating", "Swimming", "Mountain Biking", "Wheelchair", "Elliptical", "Other"};
    //private static final Double MILES_TO_KM = 1.60934;


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.delete_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.delete:
                ExerciseEntryDbHelper dbHelper = new ExerciseEntryDbHelper(this);
                Intent intent = getIntent();
                long position = intent.getLongExtra("position", 1);
                dbHelper.removeEntry(position);

                Intent returnIntent = new Intent();
                returnIntent.putExtra("databaseID",position);
                setResult(Activity.RESULT_OK,returnIntent);

                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
//        ExerciseEntryDbHelper dbHelper = new ExerciseEntryDbHelper(this);
//        Intent intent = getIntent();
//        long position = intent.getLongExtra("position", 1);
//        dbHelper.removeEntry(position);

        Intent returnIntent = new Intent();
//        returnIntent.putExtra("databaseID",position);
        setResult(Activity.RESULT_CANCELED,returnIntent);

        finish();
        super.onBackPressed();
    }

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entry_detail);

        Intent intent = getIntent();

        long position = intent.getLongExtra("position", 1);

        ExerciseEntryDbHelper dbHelper = new ExerciseEntryDbHelper(this);
        ExerciseEntry item = dbHelper.fetchEntryByIndex(position);

        EditText inputTypeText = (EditText) findViewById(R.id.input_type);
        EditText activityTypeText = (EditText) findViewById(R.id.activity_type);
        EditText dateAndTimeText = (EditText) findViewById(R.id.date_and_time);
        EditText durationText = (EditText) findViewById(R.id.duration);
        EditText distanceText = (EditText) findViewById(R.id.distance);
        EditText caloriesText = (EditText) findViewById(R.id.calories);
        EditText heartrateText = (EditText) findViewById(R.id.heartrate);

        String inputType;
        switch (item.getInputType()){
            case 0: inputType = "Manual Entry"; break;
            case 1: inputType = "GPS"; break;
            case 2: inputType = "Automatic"; break;
            default: inputType = ""; break;
        }

        String activityType;
        switch (item.getActivityType()){
            case -1: activityType = "Unknown"; break;
            default: activityType = EntryArrayAdapter.activityArray[item.getActivityType()];
        }

        Calendar time = item.getDateTime();
        String timeString = time.get(Calendar.HOUR)+":"
                +time.get(Calendar.MINUTE)+":"
                +time.get(Calendar.SECOND)+" "
                +time.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.getDefault())+" "
                +time.get(Calendar.DAY_OF_MONTH)+" "
                +time.get(Calendar.YEAR);

        String duration = "";
        int hr=0;
        int min=0;
        if (item.getDuration() >= 60*60){
            hr = item.getDuration()/(60*60);
            duration += String.valueOf(hr) + " hrs ";
        }
        if (item.getDuration() >= 60){
            min = (item.getDuration() - hr*(60*60))/60;
            duration += String.valueOf(min) + " mins ";
        }
        duration += String.valueOf(item.getDuration() - hr*(60*60) - min*60) + " secs";

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String unitpref = prefs.getString("preference_unit", "0");
        String distanceString = "";
        if (unitpref.equals("0")){
            distanceString = (new DecimalFormat("##.##")).format(item.getDistance() * EntryArrayAdapter.MILES_TO_KM) + " Kilometers";

        }else{
            distanceString = (new DecimalFormat("##.##")).format(item.getDistance()) + " Kilometers";
        }

        inputTypeText.setText(inputType);
        activityTypeText.setText(activityType);
        dateAndTimeText.setText(timeString);
        durationText.setText(duration);
        distanceText.setText(distanceString);
        caloriesText.setText(String.valueOf(item.getCalorie())+" cals");
        heartrateText.setText(String.valueOf(item.getHeartRate()) + " bpm");


    }

}
