package edu.dartmouth.cs.actiontabs;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by gary on 2/8/16.
 */
public class MapDetailActivity extends AppCompatActivity {

    private String TAG = "MapDetailActivity";

    private GoogleMap mMap;

    private ExerciseEntryDbHelper mDbHelper;
    private ExerciseEntry mEntry;
    private Intent mTrackingServiceIntent; // for tracking service
    private ServiceConnection mServiceConnection;
    /*
    *   MAP Components
    * */
    private Polyline mPolyline;
    private Marker mStartMarker;
    private Marker mEndMarker;

    private TextView mTypeText;
    private TextView mAvgSpeedText;
    private TextView mClimbText;
    private TextView mCalorieText;
    private TextView mDistanceText;



    @Override
    public void onBackPressed() {
//        ExerciseEntryDbHelper dbHelper = new ExerciseEntryDbHelper(this);
//        Intent intent = getIntent();
//        long position = intent.getLongExtra("position", 1);
//        dbHelper.removeEntry(position);

        Intent returnIntent = new Intent();
//        returnIntent.putExtra("databaseID",position);
        setResult(Activity.RESULT_CANCELED,returnIntent);

        finish();
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.delete_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.delete:
                ExerciseEntryDbHelper dbHelper = new ExerciseEntryDbHelper(this);
                Intent intent = getIntent();
                long position = intent.getLongExtra("position", 1);
                dbHelper.removeEntry(position);

                Intent returnIntent = new Intent();
                returnIntent.putExtra("databaseID",position);
                setResult(Activity.RESULT_OK,returnIntent);

                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }



//    /** Called when the activity is first created. */
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_entry_detail);
//
//        Intent intent = getIntent();
//
//        long position = intent.getLongExtra("position", 1);
//
//        ExerciseEntryDbHelper dbHelper = new ExerciseEntryDbHelper(this);
//        ExerciseEntry entry = dbHelper.fetchEntryByIndex(position);
//
//        EditText activityType = (EditText) findViewById(R.id.activity_type);
//        EditText dateAndTime = (EditText) findViewById(R.id.date_and_time);
//        EditText duration = (EditText) findViewById(R.id.duration);
//        EditText distance = (EditText) findViewById(R.id.distance);
//
//        String[] activityArray = {"Running", "Walking", "Standing","Cycling", "Hiking", "Downhill Skiing", "Cross-Country Skiing", "Snowboarding", "Skating", "Swimming", "Mountain Biking", "Wheelchair", "Elliptical", "Other"};
//        String activityTypeString = activityArray[entry.getActivityType()];
//        activityType.setText(activityTypeString);
//
//        String time = entry.getDateTime().getTime().toString();
//        dateAndTime.setText(time);
//
//
//
//        String distanceString = String.valueOf(entry.getDistance()) + " Miles";
//
//        double minutes = Math.floor(entry.getDuration());
//        double seconds = Math.floor((entry.getDuration()  - Math.floor(entry.getDuration())) * 60);
//
//        String timeString = String.valueOf(minutes + " mins " + seconds + " secs");
//
//
//        duration.setText(timeString);
//
//        distance.setText(distanceString);
//
//
//    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps_entry);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        //mapFragment.getMapAsync(this);

        Intent intent = getIntent();
        long position = intent.getLongExtra("position", 1);

        mDbHelper = new ExerciseEntryDbHelper(this);
        mEntry = mDbHelper.fetchEntryByIndex(position);
        setUpMapIfNeeded();
        drawTraceOnMap();

        mTypeText = (TextView) findViewById(R.id.maps_entry_type_text);
        mAvgSpeedText = (TextView) findViewById(R.id.maps_entry_avgspeed_text);
        mClimbText = (TextView) findViewById(R.id.maps_entry_climb_text);
        mCalorieText = (TextView) findViewById(R.id.maps_entry_calorie_text);
        mDistanceText = (TextView) findViewById(R.id.maps_entry_distance_text);
        updateStats();

    } // onCreate End



        /*
    *   LIFECYCLE OVERLOAD END
    * */

    /*
    *   MAP MANAGEMENT
    * */

    private void setUpMapIfNeeded(){
        Log.d(TAG, "setUpMapIfNeeded");
        if (mMap == null){
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
            if (mMap != null){
                setUpMap();
            }
        }
    }

    private void setUpMap(){
        Log.d(TAG, "setUpMap");
        mMap.addMarker(new MarkerOptions().position(new LatLng(0, 0)));
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mPolyline = mMap.addPolyline(new PolylineOptions());
    }

    private void drawTraceOnMap(){
        Log.d(TAG, "drawTraceOnMap");
        // validity check
        if (mEntry != null && mEntry.getLocationList().size()>0){
            Log.d(TAG, "inner drawtrace");
            ArrayList<LatLng> locations = mEntry.getLocationList();

            Log.d(TAG, "drawTraceOnMap here " + locations.get(locations.size()-1).latitude );

            // SET START
            if (mStartMarker == null){
                mStartMarker = mMap.addMarker(new MarkerOptions().position(locations.get(0)));
                mStartMarker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
            }

            // SET END
            if (mEndMarker == null){
                mEndMarker = mMap.addMarker(new MarkerOptions().position(locations.get(locations.size()-1)));
            }
            mEndMarker.setPosition(locations.get(locations.size()-1));

            // SET POLYLINE
            mPolyline.setPoints(locations);

            // CAMERA MOVE
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mEndMarker.getPosition(), 17.0f));
        }

    }

    /*
    *   MAP MANAGEMENT END
    * */

    private void updateStats(){
        Log.d(TAG, "updateStats");
        if (mEntry != null){
            String typeString = "Unknown";

            if (mEntry.getActivityType() != -1){
                typeString = (getResources().getStringArray(R.array.activity_type_array))[mEntry.getActivityType()];
            }

            mTypeText.setText("Type:" + typeString);
            mTypeText.setTextColor(Color.BLACK);
            mAvgSpeedText.setText("Avg speed: " + (new DecimalFormat("##.##")).format(mEntry.getAvgSpeed()) + " m/h");
            mAvgSpeedText.setTextColor(Color.BLACK);
            mClimbText.setText("Climb: " + (new DecimalFormat("##.##")).format(mEntry.getClimb()) + " Miles");
            mClimbText.setTextColor(Color.BLACK);
            mCalorieText.setText("Calories: " + String.valueOf(mEntry.getCalorie()));
            mCalorieText.setTextColor(Color.BLACK);
            mDistanceText.setText("Distance: " + (new DecimalFormat("##.##")).format(mEntry.getDistance()) + " Miles");
            mDistanceText.setTextColor(Color.BLACK);
        }

    }




}
