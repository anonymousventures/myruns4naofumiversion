package edu.dartmouth.cs.actiontabs;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import edu.dartmouth.cs.actiontabs.view.SlidingTabLayout;

public class MainActivity extends Activity {
    private SlidingTabLayout slidingTabLayout;
    private ViewPager viewPager;
    private ArrayList<Fragment> fragments;
    private ActionTabsViewPagerAdapter myViewPageAdapter;


//    protected ArrayList<ExerciseEntry> entries;
//
//
//    protected ArrayAdapter<ExerciseEntry> mAdapter;



	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

        setHasEmbeddedTabs(getActionBar(), true);

        // Define SlidingTabLayout (shown at top)
        // and ViewPager (shown at bottom) in the layout.
        // Get their instances.
        slidingTabLayout = (SlidingTabLayout) findViewById(R.id.tab);
        viewPager = (ViewPager) findViewById(R.id.viewpager);

        // create a fragment list in order.
        fragments = new ArrayList<Fragment>();
        fragments.add(new StartFragment());
        fragments.add(new HistoryFragment());
        fragments.add(new SettingsFragment());



        // use FragmentPagerAdapter to bind the slidingTabLayout (tabs with different titles)
        // and ViewPager (different pages of fragment) together.
        myViewPageAdapter =new ActionTabsViewPagerAdapter(getFragmentManager(),
                fragments);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener(){


            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                FragmentInterface fragment = (FragmentInterface) myViewPageAdapter.instantiateItem(viewPager,position);
                if (fragment != null){
                    fragment.fragmentShown();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });





        viewPager.setAdapter(myViewPageAdapter);

        // make sure the tabs are equally spaced.
        slidingTabLayout.setDistributeEvenly(true);
        slidingTabLayout.setViewPager(viewPager);

	}

    public interface FragmentInterface{

        void fragmentShown();

    }





    public void start(View view) {

        //create input type spinner

        //get spinner from xml
        Spinner mInputTypeSpinner =(Spinner) findViewById(R.id.input_type_spinner);
        //get text of spinner item
        String inputType = mInputTypeSpinner.getSelectedItem().toString();


        //get activity type spinner value
        //get spinner from xml
        Spinner activityTypeSpinner =(Spinner) findViewById(R.id.activity_type_spinner);
        //get text of spinner item
        String activityType = activityTypeSpinner.getSelectedItem().toString();


        //if spinner selection is manual
        if (inputType.equals("MANUAL")) {

            //start manualentryactivity
            Intent intent = new Intent(this, ManualEntryActivity.class);
            intent.putExtra("activityType", activityTypeSpinner.getSelectedItemPosition());
            startActivityForResult(intent, 1);
        }
        //if spinner selection is gps
        else if (inputType.equals("GPS")) {

            //start gpsentryactivity
            Intent intent = new Intent(this, MapsDisplayActivity.class);
            intent.putExtra("activityType", activityTypeSpinner.getSelectedItemPosition());
            intent.putExtra("inputType", 1); // 1 for gps
            startActivity(intent);
        }
        //if spinner selection is automatic
        else{

            //start automatic entry activity
            Intent intent = new Intent(this, MapsDisplayActivity.class);
            intent.putExtra("activityType", activityTypeSpinner.getSelectedItemPosition());
            intent.putExtra("inputType", 2); // 2 for automatic
            startActivity(intent);
        }

    }



//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//
//        if (requestCode == 1) {
//            if(resultCode == Activity.RESULT_OK){
//                String result=data.getStringExtra("result");
//                long databaseID = data.getLongExtra("databaseID", 1);
//
//                ExerciseEntryDbHelper exerciseEntryDbHelper = new ExerciseEntryDbHelper(this);
//                ExerciseEntry addedEntry = exerciseEntryDbHelper.fetchEntryByIndex(databaseID);
//                mAdapter.add(addedEntry);
//                mAdapter.notifyDataSetChanged();
//
//
//            }
//            if (resultCode == Activity.RESULT_CANCELED) {
//
//                // do nothing
//
//            }
//        }
//
//        if (requestCode == 2) {
//            if(resultCode == Activity.RESULT_OK){
//
//
//                System.out.println("inside this other function request code 2");
//
//                int position = data.getIntExtra("position", 1);
//
//                ExerciseEntryDbHelper exerciseEntryDbHelper = new ExerciseEntryDbHelper(this);
//                ExerciseEntry addedEntry = exerciseEntryDbHelper.fetchEntryByIndex(position);
//
//
//
//                mAdapter.remove(addedEntry);
//                mAdapter.notifyDataSetChanged();
//
//
//            }
//            if (resultCode == Activity.RESULT_CANCELED) {
//
//                // do nothing
//
//            }
//        }
//
//
//    }

    public static void setHasEmbeddedTabs(Object inActionBar, final boolean inHasEmbeddedTabs)
    {
        // get the ActionBar class
        Class<?> actionBarClass = inActionBar.getClass();

        // if it is a Jelly Bean implementation (ActionBarImplJBMR2), get the super super class (ActionBarImplICS)
        if ("android.support.v7.app.ActionBarImplJBMR2".equals(actionBarClass.getName()))
        {
            actionBarClass = actionBarClass.getSuperclass().getSuperclass();
        }
        // if it is a Jelly Bean implementation (ActionBarImplJB), get the super class (ActionBarImplICS)
        else if ("android.support.v7.app.ActionBarImplJB".equals(actionBarClass.getName()))
        {
            actionBarClass = actionBarClass.getSuperclass();
        }

        try
        {
            // try to get the mActionBar field, because the current ActionBar is probably just a wrapper Class
            // if this fails, no worries, this will be an instance of the native ActionBar class or from the ActionBarImplBase class
            final Field actionBarField = actionBarClass.getDeclaredField("mActionBar");
            actionBarField.setAccessible(true);
            inActionBar = actionBarField.get(inActionBar);
            actionBarClass = inActionBar.getClass();
        }
        catch (IllegalAccessException e) {}
        catch (IllegalArgumentException e) {}
        catch (NoSuchFieldException e) {}

        try
        {
            // now call the method setHasEmbeddedTabs, this will put the tabs inside the ActionBar
            // if this fails, you're on you own <img draggable="false" class="emoji" alt="😉" src="http://s.w.org/images/core/emoji/72x72/1f609.png" scale="0">
            final Method method = actionBarClass.getDeclaredMethod("setHasEmbeddedTabs", new Class[] { Boolean.TYPE });
            method.setAccessible(true);
            method.invoke(inActionBar, new Object[]{ inHasEmbeddedTabs });
        }
        catch (NoSuchMethodException e)        {}
        catch (InvocationTargetException e) {}
        catch (IllegalAccessException e) {}
        catch (IllegalArgumentException e) {}
    }






}