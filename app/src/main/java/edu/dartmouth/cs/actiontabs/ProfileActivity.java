package edu.dartmouth.cs.actiontabs;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import com.soundcloud.android.crop.Crop;

import static android.app.PendingIntent.getActivity;


public class ProfileActivity extends Activity {
    
    public static final int REQUEST_CODE_TAKE_FROM_CAMERA = 0;
    public static final int REQUEST_CODE_TAKE_FROM_GALLERY = 1;
    public static final int REQUEST_CODE_CROP_PHOTO = 2;

    private static final String IMAGE_UNSPECIFIED = "image/*";
    private static final String URI_INSTANCE_STATE_KEY = "saved_uri";
    private static final String CAMERA_TAKEN_KEY = "camera_taken";
    private static final String GALLERY_TAKEN_KEY = "gallery_taken";

    private Uri mImageTempUri;
    private Uri mImageCaptureUri;
    private ImageView mImageView;
    private boolean isTakenFromCamera;
    private boolean isTakenFromGallery;


    //other variables
    private EditText nameText;
    private EditText emailText;
    private EditText phoneText;
    private EditText classText;
    private EditText majorText;
    private RadioGroup genderSelection;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        mImageView = (ImageView) findViewById(R.id.imageProfile);
        isTakenFromCamera = false;
        isTakenFromGallery = false;

        if (savedInstanceState != null) {
            mImageCaptureUri = savedInstanceState
                    .getParcelable(URI_INSTANCE_STATE_KEY);
            isTakenFromCamera = savedInstanceState.getBoolean(CAMERA_TAKEN_KEY);
            isTakenFromGallery = savedInstanceState.getBoolean(GALLERY_TAKEN_KEY);
        }

        nameText = (EditText) findViewById(R.id.name);
        emailText = (EditText) findViewById(R.id.email);
        phoneText = (EditText) findViewById(R.id.phone);
        classText = (EditText) findViewById(R.id.year);
        majorText = (EditText) findViewById(R.id.major);
        genderSelection = (RadioGroup) findViewById(R.id.gender_selection);

        loadSnap();

        loadProfile();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(URI_INSTANCE_STATE_KEY, mImageCaptureUri);
        outState.putBoolean(CAMERA_TAKEN_KEY, isTakenFromCamera);
        outState.putBoolean(GALLERY_TAKEN_KEY, isTakenFromGallery);

    }

    // ****************** button click callbacks ***************************//

    public void onSaveClicked(View v) {
        // Save picture
        saveSnap();
        // Making a "toast" informing the user the picture is saved.
        Toast.makeText(getApplicationContext(),
                getString(R.string.ui_profile_toast_save_text),
                Toast.LENGTH_SHORT).show();

        // Close the activity
        finish();
    }

    public void onChangePhotoClicked(View v) {
        // changing the profile image, show the dialog asking the user
        // to choose between taking a picture
        // Go to MyRunsDialogFragment for details.

        displayDialog(MyRunsDialogFragment.DIALOG_ID_PHOTO_PICKER);
        Log.d("testing", "hello here");
    }

    // Handle data after activity returns.
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK)
            return;

        switch (requestCode) {
            case REQUEST_CODE_TAKE_FROM_CAMERA:
                // Send image taken from camera for cropping
                beginCrop(mImageCaptureUri);
                break;

            case REQUEST_CODE_TAKE_FROM_GALLERY:
                mImageCaptureUri = data.getData();
                beginCrop(mImageCaptureUri);
                break;

            case Crop.REQUEST_CROP: //We changed the RequestCode to the one being used by the library.
                // Update image view after image crop
                handleCrop(resultCode, data);

                // Delete temporary image taken by camera after crop.
                if (isTakenFromCamera) {
                    File f = new File(mImageTempUri.getPath());
                    if (f.exists())
                        f.delete();
                }

                break;
        }
    }

    // ******* Photo picker dialog related functions ************//

    public void displayDialog(int id) {
        DialogFragment fragment = MyRunsDialogFragment.newInstance(id);
        fragment.show(getFragmentManager(),
                getString(R.string.dialog_fragment_tag_photo_picker));
    }

    public void onPhotoPickerItemSelected(int item) {
        Intent intent;

        switch (item) {

            case MyRunsDialogFragment.ID_PHOTO_PICKER_FROM_CAMERA:
                // Take photo from camera，
                // Construct an intent with action
                // MediaStore.ACTION_IMAGE_CAPTURE
                intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                // Construct temporary image path and name to save the taken
                // photo
                mImageCaptureUri = Uri.fromFile(new File(Environment
                        .getExternalStorageDirectory(), "tmp_"
                        + String.valueOf(System.currentTimeMillis()) + ".jpg"));
                intent.putExtra(MediaStore.EXTRA_OUTPUT,
                        mImageCaptureUri);
                intent.putExtra("return-data", true);
                try {
                    // Start a camera capturing activity
                    // REQUEST_CODE_TAKE_FROM_CAMERA is an integer tag you
                    // defined to identify the activity in onActivityResult()
                    // when it returns
                    startActivityForResult(intent, REQUEST_CODE_TAKE_FROM_CAMERA);
                } catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                }
                isTakenFromCamera = true;
                break;

            case MyRunsDialogFragment.ID_PHOTO_PICKER_FROM_GALLERY:

                intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                try {
                    // Start a camera capturing activity
                    // REQUEST_CODE_TAKE_FROM_CAMERA is an integer tag you
                    // defined to identify the activity in onActivityResult()
                    // when it returns
                    startActivityForResult(intent, REQUEST_CODE_TAKE_FROM_GALLERY);
                } catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                }
                isTakenFromGallery = true;

                break;

            default:
                return;
        }

    }

    // ****************** private helper functions ***************************//

    private void loadSnap() {
        // Load profile photo from internal storage
        try {
            if (isTakenFromCamera || isTakenFromGallery){
                mImageView.setImageResource(0);
                mImageView.setImageURI(mImageCaptureUri);
            }else{
                FileInputStream fis = openFileInput(getString(R.string.profile_photo_file_name));
                Bitmap bmap = BitmapFactory.decodeStream(fis);
                mImageView.setImageResource(0);
                mImageView.setImageBitmap(bmap);
                fis.close();
            }
        } catch (IOException e) {
            // Default profile photo if no photo saved before.
            mImageView.setImageResource(0);
            mImageView.setImageResource(R.drawable.ic_cs);
        }
    }

    private void saveSnap() {

        // Commit all the changes into preference file
        // Save profile image into internal storage.
        mImageView.buildDrawingCache();
        Bitmap bmap = mImageView.getDrawingCache();
        try {
            FileOutputStream fos = openFileOutput(
                    getString(R.string.profile_photo_file_name), MODE_PRIVATE);
            bmap.compress(Bitmap.CompressFormat.PNG, 100, fos);

            Log.d("file output", fos.toString());
            fos.flush();
            fos.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    // Crop and resize the image for profile
    private void cropImage() {
        // Use existing crop activity.
        Log.d("cropImage", mImageCaptureUri.toString());

        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(mImageCaptureUri, IMAGE_UNSPECIFIED);

        // Specify image size
        intent.putExtra("outputX", 100);
        intent.putExtra("outputY", 100);

        // Specify aspect ratio, 1:1
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        intent.putExtra("scale", true);
        intent.putExtra("return-data", true);
        // REQUEST_CODE_CROP_PHOTO is an integer tag you defined to
        // identify the activity in onActivityResult() when it returns
        startActivityForResult(intent, REQUEST_CODE_CROP_PHOTO);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void cancel(View view) {

        Context context = getApplicationContext();
        CharSequence text = "Cancelled";
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, text, duration);
        toast.show();

        finish();
    }

    public void saveProfile(View view) {

        saveSnap();

        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(getString(R.string.name), nameText.getText().toString());
        editor.putString(getString(R.string.email), emailText.getText().toString());
        editor.putString(getString(R.string.phone), phoneText.getText().toString());
        editor.putString(getString(R.string.year), classText.getText().toString());
        editor.putString(getString(R.string.major), majorText.getText().toString());


        int selectedId = genderSelection.indexOfChild(findViewById(genderSelection.getCheckedRadioButtonId()));

        editor.putInt(getString(R.string.gender), selectedId);

        Log.d("saved profile", Integer.toString(selectedId));

        editor.commit();

        Context context = getApplicationContext();
        CharSequence text = "Saved";
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, text, duration);
        toast.show();

        Log.d("saved profile", "ran");

        finish();
    }

    public void loadProfile() {

        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);

        String prefNameText =  sharedPref.getString(getString(R.string.name), "");
        String prefEmailText = sharedPref.getString(getString(R.string.email), "");
        String prefPhoneText = sharedPref.getString(getString(R.string.phone), "");
        String prefClassText = sharedPref.getString(getString(R.string.year), "");
        String prefMajorText = sharedPref.getString(getString(R.string.major), "");
        Integer selectedId = sharedPref.getInt(getString(R.string.gender), 0);

        nameText.setText(prefNameText);
        emailText.setText(prefEmailText);
        phoneText.setText(prefPhoneText);
        classText.setText(prefClassText);
        majorText.setText(prefMajorText);
        ((RadioButton)genderSelection.getChildAt(selectedId)).setChecked(true);

    }


    /** Method to start Crop activity using the library
     *	Earlier the code used to start a new intent to crop the image,
     *	but here the library is handling the creation of an Intent, so you don't
     * have to.
     *  **/
    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped"));
        mImageTempUri = source;
        Crop.of(source, destination).asSquare().start(this);
    }

    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == RESULT_OK) {
            mImageCaptureUri = Crop.getOutput(result);
            mImageView.setImageResource(0);
            mImageView.setImageURI(mImageCaptureUri);
        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(this, Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }



}
