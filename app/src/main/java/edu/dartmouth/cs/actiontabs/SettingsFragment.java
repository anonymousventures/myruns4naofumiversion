package edu.dartmouth.cs.actiontabs;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.preference.PreferenceFragment;

// preference structure is in xml file, under /xml/preference.xml

public class SettingsFragment extends PreferenceFragment implements MainActivity.FragmentInterface{
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preference);
    }

    public void fragmentShown(){

    }

}

