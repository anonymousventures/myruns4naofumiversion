package edu.dartmouth.cs.actiontabs;

import android.content.AsyncTaskLoader;
import android.content.Context;

import java.util.ArrayList;

/**
 * Created by gary on 1/31/16.
 */

public class ExerciseEntryLoader extends AsyncTaskLoader<ArrayList<ExerciseEntry>> {
    private Context c;

    public ExerciseEntryLoader(Context context) {
        super(context);
        c = context;
    }
    @Override
    public ArrayList<ExerciseEntry> loadInBackground() {
        ExerciseEntryDbHelper exerciseEntryDbHelper = new ExerciseEntryDbHelper(c);
        ArrayList<ExerciseEntry> entries = exerciseEntryDbHelper.fetchEntries();

        return entries;
    }


}