package edu.dartmouth.cs.actiontabs;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by Naofumi on 1/29/16.
 * Note: DBhelper is for managing database itself, not data in it.
 */


public class ExerciseEntryDbHelper extends SQLiteOpenHelper {

    public static final String CREATE_TABLE_ENTRIES = "CREATE TABLE IF NOT EXISTS ENTRIES(_id INTEGER PRIMARY KEY AUTOINCREMENT, input_type INTEGER NOT NULL, activity_type INTEGER NOT NULL, date_time DATETIME NOT NULL, duration INTEGER NOT NULL, distance FLOAT, avg_pace FLOAT, avg_speed FLOAT, calories INTEGER, climb FLOAT, heartrate INTEGER, comment TEXT, privacy INTEGER, gps_data BLOB)";
    public static final String DATABASE_NAME = "exerciseDB";
    public static final String TABLE_NAME = "ENTRIES";
    public static final int DATABASE_VERSION = 1;

    public static final String ID_COLUMN = "_id";
    public static final String INPUT_TYPE_COLUMN = "input_type";
    public static final String ACTIVITY_TYPE_COLUMN = "activity_type";
    public static final String DATE_TIME_COLUMN = "date_time";
    public static final String DURATION_COLUMN = "duration";
    public static final String DISTANCE_COLUMN = "distance";
    public static final String AVG_PACE_COLUMN = "avg_pace";
    public static final String AVG_SPEED_COLUMN = "avg_speed";
    public static final String CALORIES_COLUMN = "calories";
    public static final String CLIMB_COLUMN = "climb";
    public static final String HEART_RATE_COLUMN = "heartrate";
    public static final String COMMENT_COLUMN = "comment";
    public static final String PRIVACY_COLUMN = "privacy";
    public static final String GPS_DATA_COLUMN = "gps_data";

    public ExerciseEntryDbHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public long insertEntry(ExerciseEntry entry){
        ContentValues values = new ContentValues();
        values.put(INPUT_TYPE_COLUMN, entry.getInputType());
        values.put(ACTIVITY_TYPE_COLUMN, entry.getActivityType());
        values.put(DATE_TIME_COLUMN, entry.getDateTime().getTimeInMillis());
        values.put(DURATION_COLUMN, entry.getDuration());
        values.put(DISTANCE_COLUMN, entry.getDistance());
        values.put(AVG_PACE_COLUMN, entry.getAvgPace());
        values.put(AVG_SPEED_COLUMN, entry.getAvgSpeed());
        values.put(CALORIES_COLUMN, entry.getCalorie());
        values.put(CLIMB_COLUMN, entry.getClimb());
        values.put(HEART_RATE_COLUMN, entry.getHeartRate());
        values.put(COMMENT_COLUMN, entry.getComment());
        values.put(GPS_DATA_COLUMN, entry.getLocationByteArray());
        // mLocationList not available at this moment
        SQLiteDatabase db = getWritableDatabase();
        try {
            return db.insert(TABLE_NAME, null, values);
        } finally {
            db.close();
        }
    }

    public void removeEntry(long rowIndex){
        SQLiteDatabase db = getWritableDatabase();
        db.delete(TABLE_NAME, "_id="+rowIndex, null);
        db.close();
    }

    public void removeEntry(ExerciseEntry entry){
        removeEntry(entry.getId());
    }

    public ExerciseEntry fetchEntryByIndex(long rowId){
        SQLiteDatabase db = getReadableDatabase();
        // Note: query(table, columns, whereClause, whereArgs, groupBy, having, orderBy)
        Cursor cursor = db.query(TABLE_NAME, null, "_id="+rowId, null, null, null, null, null);
        try{
            if (cursor.getCount() == 0){
                return null;
            }
            cursor.moveToFirst();
            ExerciseEntry entry = getEntry(cursor);
            return entry;
        } finally {
            cursor.close();
            db.close();
        }
    }

    public ArrayList<ExerciseEntry> fetchEntries(){
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME, null, null, null, null, null, null, null);
        ArrayList<ExerciseEntry> entries = new ArrayList<>();
        try{
            cursor.moveToFirst();
            while(! cursor.isAfterLast()){
                entries.add(getEntry(cursor));
                cursor.moveToNext();
            }
            return entries;
        }finally {
            cursor.close();
        }
    }

    // Helper function: converting cursor into ExerciseEntry
    private ExerciseEntry getEntry(Cursor cursor){
        ExerciseEntry entry = new ExerciseEntry();
        entry.setId(Long.valueOf(cursor.getLong(cursor.getColumnIndex(ID_COLUMN))));
        entry.setInputType(cursor.getInt(cursor.getColumnIndex(INPUT_TYPE_COLUMN)));
        entry.setActivityType(cursor.getInt(cursor.getColumnIndex(ACTIVITY_TYPE_COLUMN)));
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(cursor.getLong(cursor.getColumnIndex(DATE_TIME_COLUMN)));
        entry.setDateTime(calendar);
        entry.setDuration(cursor.getInt(cursor.getColumnIndex(DURATION_COLUMN)));
        entry.setDistance(cursor.getDouble(cursor.getColumnIndex(DISTANCE_COLUMN)));
        entry.setAvgPace(cursor.getDouble(cursor.getColumnIndex(AVG_PACE_COLUMN)));
        entry.setAvgSpeed(cursor.getDouble(cursor.getColumnIndex(AVG_SPEED_COLUMN)));
        entry.setCalorie(cursor.getInt(cursor.getColumnIndex(CALORIES_COLUMN)));
        entry.setClimb(cursor.getDouble(cursor.getColumnIndex(CLIMB_COLUMN)));
        entry.setHeartRate(cursor.getInt(cursor.getColumnIndex(HEART_RATE_COLUMN)));
        entry.setComment(cursor.getString(cursor.getColumnIndex(COMMENT_COLUMN)));
        if (cursor.getBlob(cursor.getColumnIndex(GPS_DATA_COLUMN)) != null){
            entry.setLocationListFromByteArray(cursor.getBlob(cursor.getColumnIndex(GPS_DATA_COLUMN)));
        }

        // mlocationlist not available this moment

        return entry;
    }

}
