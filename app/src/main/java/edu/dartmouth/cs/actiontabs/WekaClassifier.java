package edu.dartmouth.cs.actiontabs;


class WekaClassifier {

    public static double classify(Object[] i)
            throws Exception {

        double p = Double.NaN;
        p = WekaClassifier.N73e0638d155(i);
        return p;
    }
    static double N73e0638d155(Object []i) {
        double p = Double.NaN;
        if (i[0] == null) {
            p = 0;
        } else if (((Double) i[0]).doubleValue() <= 174.792423) {
            p = WekaClassifier.N10a7f96a156(i);
        } else if (((Double) i[0]).doubleValue() > 174.792423) {
            p = WekaClassifier.N5ce7ac01294(i);
        }
        return p;
    }
    static double N10a7f96a156(Object []i) {
        double p = Double.NaN;
        if (i[20] == null) {
            p = 0;
        } else if (((Double) i[20]).doubleValue() <= 0.351595) {
            p = WekaClassifier.N3e7a6b1157(i);
        } else if (((Double) i[20]).doubleValue() > 0.351595) {
            p = WekaClassifier.N6914225f204(i);
        }
        return p;
    }
    static double N3e7a6b1157(Object []i) {
        double p = Double.NaN;
        if (i[9] == null) {
            p = 0;
        } else if (((Double) i[9]).doubleValue() <= 1.351291) {
            p = WekaClassifier.N5d70b077158(i);
        } else if (((Double) i[9]).doubleValue() > 1.351291) {
            p = WekaClassifier.N4c2a7392189(i);
        }
        return p;
    }
    static double N5d70b077158(Object []i) {
        double p = Double.NaN;
        if (i[22] == null) {
            p = 0;
        } else if (((Double) i[22]).doubleValue() <= 0.388863) {
            p = WekaClassifier.N1151848e159(i);
        } else if (((Double) i[22]).doubleValue() > 0.388863) {
            p = WekaClassifier.N361cbdff182(i);
        }
        return p;
    }
    static double N1151848e159(Object []i) {
        double p = Double.NaN;
        if (i[64] == null) {
            p = 0;
        } else if (((Double) i[64]).doubleValue() <= 0.289909) {
            p = 0;
        } else if (((Double) i[64]).doubleValue() > 0.289909) {
            p = WekaClassifier.N7ed58f83160(i);
        }
        return p;
    }
    static double N7ed58f83160(Object []i) {
        double p = Double.NaN;
        if (i[0] == null) {
            p = 0;
        } else if (((Double) i[0]).doubleValue() <= 22.821745) {
            p = WekaClassifier.N76c12282161(i);
        } else if (((Double) i[0]).doubleValue() > 22.821745) {
            p = WekaClassifier.N35e0932a172(i);
        }
        return p;
    }
    static double N76c12282161(Object []i) {
        double p = Double.NaN;
        if (i[0] == null) {
            p = 0;
        } else if (((Double) i[0]).doubleValue() <= 10.812382) {
            p = 0;
        } else if (((Double) i[0]).doubleValue() > 10.812382) {
            p = WekaClassifier.N1c0c1b7162(i);
        }
        return p;
    }
    static double N1c0c1b7162(Object []i) {
        double p = Double.NaN;
        if (i[22] == null) {
            p = 0;
        } else if (((Double) i[22]).doubleValue() <= 0.293373) {
            p = WekaClassifier.N15edd647163(i);
        } else if (((Double) i[22]).doubleValue() > 0.293373) {
            p = 0;
        }
        return p;
    }
    static double N15edd647163(Object []i) {
        double p = Double.NaN;
        if (i[3] == null) {
            p = 0;
        } else if (((Double) i[3]).doubleValue() <= 0.342481) {
            p = 0;
        } else if (((Double) i[3]).doubleValue() > 0.342481) {
            p = WekaClassifier.N318eda90164(i);
        }
        return p;
    }
    static double N318eda90164(Object []i) {
        double p = Double.NaN;
        if (i[12] == null) {
            p = 0;
        } else if (((Double) i[12]).doubleValue() <= 0.39899) {
            p = WekaClassifier.N19af3d5b165(i);
        } else if (((Double) i[12]).doubleValue() > 0.39899) {
            p = 0;
        }
        return p;
    }
    static double N19af3d5b165(Object []i) {
        double p = Double.NaN;
        if (i[1] == null) {
            p = 0;
        } else if (((Double) i[1]).doubleValue() <= 0.597044) {
            p = WekaClassifier.N3fe52e89166(i);
        } else if (((Double) i[1]).doubleValue() > 0.597044) {
            p = WekaClassifier.N78bf10f8169(i);
        }
        return p;
    }
    static double N3fe52e89166(Object []i) {
        double p = Double.NaN;
        if (i[3] == null) {
            p = 0;
        } else if (((Double) i[3]).doubleValue() <= 0.802374) {
            p = 0;
        } else if (((Double) i[3]).doubleValue() > 0.802374) {
            p = WekaClassifier.N5f524524167(i);
        }
        return p;
    }
    static double N5f524524167(Object []i) {
        double p = Double.NaN;
        if (i[23] == null) {
            p = 1;
        } else if (((Double) i[23]).doubleValue() <= 0.095958) {
            p = WekaClassifier.N12396f60168(i);
        } else if (((Double) i[23]).doubleValue() > 0.095958) {
            p = 0;
        }
        return p;
    }
    static double N12396f60168(Object []i) {
        double p = Double.NaN;
        if (i[13] == null) {
            p = 1;
        } else if (((Double) i[13]).doubleValue() <= 0.147162) {
            p = 1;
        } else if (((Double) i[13]).doubleValue() > 0.147162) {
            p = 0;
        }
        return p;
    }
    static double N78bf10f8169(Object []i) {
        double p = Double.NaN;
        if (i[17] == null) {
            p = 1;
        } else if (((Double) i[17]).doubleValue() <= 0.071153) {
            p = WekaClassifier.N47f347b6170(i);
        } else if (((Double) i[17]).doubleValue() > 0.071153) {
            p = 0;
        }
        return p;
    }
    static double N47f347b6170(Object []i) {
        double p = Double.NaN;
        if (i[9] == null) {
            p = 1;
        } else if (((Double) i[9]).doubleValue() <= 0.45829) {
            p = WekaClassifier.N34e922a1171(i);
        } else if (((Double) i[9]).doubleValue() > 0.45829) {
            p = 0;
        }
        return p;
    }
    static double N34e922a1171(Object []i) {
        double p = Double.NaN;
        if (i[2] == null) {
            p = 0;
        } else if (((Double) i[2]).doubleValue() <= 0.712248) {
            p = 0;
        } else if (((Double) i[2]).doubleValue() > 0.712248) {
            p = 1;
        }
        return p;
    }
    static double N35e0932a172(Object []i) {
        double p = Double.NaN;
        if (i[0] == null) {
            p = 0;
        } else if (((Double) i[0]).doubleValue() <= 88.708455) {
            p = WekaClassifier.N2addc608173(i);
        } else if (((Double) i[0]).doubleValue() > 88.708455) {
            p = 0;
        }
        return p;
    }
    static double N2addc608173(Object []i) {
        double p = Double.NaN;
        if (i[1] == null) {
            p = 0;
        } else if (((Double) i[1]).doubleValue() <= 1.572967) {
            p = 0;
        } else if (((Double) i[1]).doubleValue() > 1.572967) {
            p = WekaClassifier.N45f44d34174(i);
        }
        return p;
    }
    static double N45f44d34174(Object []i) {
        double p = Double.NaN;
        if (i[5] == null) {
            p = 0;
        } else if (((Double) i[5]).doubleValue() <= 2.794783) {
            p = WekaClassifier.N29f8b4cd175(i);
        } else if (((Double) i[5]).doubleValue() > 2.794783) {
            p = 0;
        }
        return p;
    }
    static double N29f8b4cd175(Object []i) {
        double p = Double.NaN;
        if (i[5] == null) {
            p = 0;
        } else if (((Double) i[5]).doubleValue() <= 2.607286) {
            p = WekaClassifier.N7261442176(i);
        } else if (((Double) i[5]).doubleValue() > 2.607286) {
            p = 1;
        }
        return p;
    }
    static double N7261442176(Object []i) {
        double p = Double.NaN;
        if (i[11] == null) {
            p = 1;
        } else if (((Double) i[11]).doubleValue() <= 0.136352) {
            p = 1;
        } else if (((Double) i[11]).doubleValue() > 0.136352) {
            p = WekaClassifier.N54fbfcb8177(i);
        }
        return p;
    }
    static double N54fbfcb8177(Object []i) {
        double p = Double.NaN;
        if (i[19] == null) {
            p = 3;
        } else if (((Double) i[19]).doubleValue() <= 0.123112) {
            p = WekaClassifier.N6b8cbf9e178(i);
        } else if (((Double) i[19]).doubleValue() > 0.123112) {
            p = WekaClassifier.N6578553b179(i);
        }
        return p;
    }
    static double N6b8cbf9e178(Object []i) {
        double p = Double.NaN;
        if (i[12] == null) {
            p = 0;
        } else if (((Double) i[12]).doubleValue() <= 0.182182) {
            p = 0;
        } else if (((Double) i[12]).doubleValue() > 0.182182) {
            p = 3;
        }
        return p;
    }
    static double N6578553b179(Object []i) {
        double p = Double.NaN;
        if (i[10] == null) {
            p = 0;
        } else if (((Double) i[10]).doubleValue() <= 0.754588) {
            p = WekaClassifier.N3f23c888180(i);
        } else if (((Double) i[10]).doubleValue() > 0.754588) {
            p = 0;
        }
        return p;
    }
    static double N3f23c888180(Object []i) {
        double p = Double.NaN;
        if (i[5] == null) {
            p = 0;
        } else if (((Double) i[5]).doubleValue() <= 1.57872) {
            p = 0;
        } else if (((Double) i[5]).doubleValue() > 1.57872) {
            p = WekaClassifier.N4642791a181(i);
        }
        return p;
    }
    static double N4642791a181(Object []i) {
        double p = Double.NaN;
        if (i[7] == null) {
            p = 0;
        } else if (((Double) i[7]).doubleValue() <= 0.712784) {
            p = 0;
        } else if (((Double) i[7]).doubleValue() > 0.712784) {
            p = 3;
        }
        return p;
    }
    static double N361cbdff182(Object []i) {
        double p = Double.NaN;
        if (i[7] == null) {
            p = 0;
        } else if (((Double) i[7]).doubleValue() <= 0.725602) {
            p = 0;
        } else if (((Double) i[7]).doubleValue() > 0.725602) {
            p = WekaClassifier.N18fdcc1a183(i);
        }
        return p;
    }
    static double N18fdcc1a183(Object []i) {
        double p = Double.NaN;
        if (i[27] == null) {
            p = 3;
        } else if (((Double) i[27]).doubleValue() <= 0.313665) {
            p = WekaClassifier.N3e62cbc7184(i);
        } else if (((Double) i[27]).doubleValue() > 0.313665) {
            p = WekaClassifier.N2412c49e188(i);
        }
        return p;
    }
    static double N3e62cbc7184(Object []i) {
        double p = Double.NaN;
        if (i[14] == null) {
            p = 1;
        } else if (((Double) i[14]).doubleValue() <= 0.13406) {
            p = 1;
        } else if (((Double) i[14]).doubleValue() > 0.13406) {
            p = WekaClassifier.N483095d0185(i);
        }
        return p;
    }
    static double N483095d0185(Object []i) {
        double p = Double.NaN;
        if (i[8] == null) {
            p = 0;
        } else if (((Double) i[8]).doubleValue() <= 0.589486) {
            p = 0;
        } else if (((Double) i[8]).doubleValue() > 0.589486) {
            p = WekaClassifier.N6dbbafb6186(i);
        }
        return p;
    }
    static double N6dbbafb6186(Object []i) {
        double p = Double.NaN;
        if (i[6] == null) {
            p = 0;
        } else if (((Double) i[6]).doubleValue() <= 0.700591) {
            p = WekaClassifier.N3d0d0e02187(i);
        } else if (((Double) i[6]).doubleValue() > 0.700591) {
            p = 3;
        }
        return p;
    }
    static double N3d0d0e02187(Object []i) {
        double p = Double.NaN;
        if (i[12] == null) {
            p = 3;
        } else if (((Double) i[12]).doubleValue() <= 0.322012) {
            p = 3;
        } else if (((Double) i[12]).doubleValue() > 0.322012) {
            p = 0;
        }
        return p;
    }
    static double N2412c49e188(Object []i) {
        double p = Double.NaN;
        if (i[9] == null) {
            p = 3;
        } else if (((Double) i[9]).doubleValue() <= 0.437546) {
            p = 3;
        } else if (((Double) i[9]).doubleValue() > 0.437546) {
            p = 0;
        }
        return p;
    }
    static double N4c2a7392189(Object []i) {
        double p = Double.NaN;
        if (i[0] == null) {
            p = 0;
        } else if (((Double) i[0]).doubleValue() <= 26.709249) {
            p = 0;
        } else if (((Double) i[0]).doubleValue() > 26.709249) {
            p = WekaClassifier.N77099d4e190(i);
        }
        return p;
    }
    static double N77099d4e190(Object []i) {
        double p = Double.NaN;
        if (i[0] == null) {
            p = 3;
        } else if (((Double) i[0]).doubleValue() <= 79.487692) {
            p = WekaClassifier.N1836a8f0191(i);
        } else if (((Double) i[0]).doubleValue() > 79.487692) {
            p = WekaClassifier.N1a920729203(i);
        }
        return p;
    }
    static double N1836a8f0191(Object []i) {
        double p = Double.NaN;
        if (i[11] == null) {
            p = 0;
        } else if (((Double) i[11]).doubleValue() <= 0.42159) {
            p = WekaClassifier.N2c8d30fb192(i);
        } else if (((Double) i[11]).doubleValue() > 0.42159) {
            p = WekaClassifier.N698ac596193(i);
        }
        return p;
    }
    static double N2c8d30fb192(Object []i) {
        double p = Double.NaN;
        if (i[7] == null) {
            p = 1;
        } else if (((Double) i[7]).doubleValue() <= 1.642186) {
            p = 1;
        } else if (((Double) i[7]).doubleValue() > 1.642186) {
            p = 0;
        }
        return p;
    }
    static double N698ac596193(Object []i) {
        double p = Double.NaN;
        if (i[2] == null) {
            p = 3;
        } else if (((Double) i[2]).doubleValue() <= 9.444663) {
            p = WekaClassifier.N15b230fc194(i);
        } else if (((Double) i[2]).doubleValue() > 9.444663) {
            p = WekaClassifier.N52ac016e202(i);
        }
        return p;
    }
    static double N15b230fc194(Object []i) {
        double p = Double.NaN;
        if (i[17] == null) {
            p = 1;
        } else if (((Double) i[17]).doubleValue() <= 0.052575) {
            p = 1;
        } else if (((Double) i[17]).doubleValue() > 0.052575) {
            p = WekaClassifier.N65adfb84195(i);
        }
        return p;
    }
    static double N65adfb84195(Object []i) {
        double p = Double.NaN;
        if (i[1] == null) {
            p = 3;
        } else if (((Double) i[1]).doubleValue() <= 4.110405) {
            p = 3;
        } else if (((Double) i[1]).doubleValue() > 4.110405) {
            p = WekaClassifier.N15bcb43196(i);
        }
        return p;
    }
    static double N15bcb43196(Object []i) {
        double p = Double.NaN;
        if (i[27] == null) {
            p = 0;
        } else if (((Double) i[27]).doubleValue() <= 0.136757) {
            p = 0;
        } else if (((Double) i[27]).doubleValue() > 0.136757) {
            p = WekaClassifier.N317d9c67197(i);
        }
        return p;
    }
    static double N317d9c67197(Object []i) {
        double p = Double.NaN;
        if (i[23] == null) {
            p = 3;
        } else if (((Double) i[23]).doubleValue() <= 0.359774) {
            p = WekaClassifier.N2da74793198(i);
        } else if (((Double) i[23]).doubleValue() > 0.359774) {
            p = WekaClassifier.N6b950106200(i);
        }
        return p;
    }
    static double N2da74793198(Object []i) {
        double p = Double.NaN;
        if (i[2] == null) {
            p = 3;
        } else if (((Double) i[2]).doubleValue() <= 6.011683) {
            p = 3;
        } else if (((Double) i[2]).doubleValue() > 6.011683) {
            p = WekaClassifier.N414c1b4f199(i);
        }
        return p;
    }
    static double N414c1b4f199(Object []i) {
        double p = Double.NaN;
        if (i[11] == null) {
            p = 1;
        } else if (((Double) i[11]).doubleValue() <= 1.181331) {
            p = 1;
        } else if (((Double) i[11]).doubleValue() > 1.181331) {
            p = 3;
        }
        return p;
    }
    static double N6b950106200(Object []i) {
        double p = Double.NaN;
        if (i[30] == null) {
            p = 0;
        } else if (((Double) i[30]).doubleValue() <= 0.276738) {
            p = 0;
        } else if (((Double) i[30]).doubleValue() > 0.276738) {
            p = WekaClassifier.N3766818201(i);
        }
        return p;
    }
    static double N3766818201(Object []i) {
        double p = Double.NaN;
        if (i[2] == null) {
            p = 0;
        } else if (((Double) i[2]).doubleValue() <= 2.789284) {
            p = 0;
        } else if (((Double) i[2]).doubleValue() > 2.789284) {
            p = 3;
        }
        return p;
    }
    static double N52ac016e202(Object []i) {
        double p = Double.NaN;
        if (i[15] == null) {
            p = 0;
        } else if (((Double) i[15]).doubleValue() <= 1.754653) {
            p = 0;
        } else if (((Double) i[15]).doubleValue() > 1.754653) {
            p = 1;
        }
        return p;
    }
    static double N1a920729203(Object []i) {
        double p = Double.NaN;
        if (i[2] == null) {
            p = 0;
        } else if (((Double) i[2]).doubleValue() <= 26.972603) {
            p = 0;
        } else if (((Double) i[2]).doubleValue() > 26.972603) {
            p = 1;
        }
        return p;
    }
    static double N6914225f204(Object []i) {
        double p = Double.NaN;
        if (i[0] == null) {
            p = 0;
        } else if (((Double) i[0]).doubleValue() <= 108.362354) {
            p = WekaClassifier.N28dcc0de205(i);
        } else if (((Double) i[0]).doubleValue() > 108.362354) {
            p = WekaClassifier.N11c692c2275(i);
        }
        return p;
    }
    static double N28dcc0de205(Object []i) {
        double p = Double.NaN;
        if (i[0] == null) {
            p = 0;
        } else if (((Double) i[0]).doubleValue() <= 10.615099) {
            p = 0;
        } else if (((Double) i[0]).doubleValue() > 10.615099) {
            p = WekaClassifier.N34fa43c7206(i);
        }
        return p;
    }
    static double N34fa43c7206(Object []i) {
        double p = Double.NaN;
        if (i[1] == null) {
            p = 3;
        } else if (((Double) i[1]).doubleValue() <= 0.839325) {
            p = WekaClassifier.N1a77d8fd207(i);
        } else if (((Double) i[1]).doubleValue() > 0.839325) {
            p = WekaClassifier.N3eda33d1211(i);
        }
        return p;
    }
    static double N1a77d8fd207(Object []i) {
        double p = Double.NaN;
        if (i[20] == null) {
            p = 0;
        } else if (((Double) i[20]).doubleValue() <= 0.658401) {
            p = WekaClassifier.N3246d79e208(i);
        } else if (((Double) i[20]).doubleValue() > 0.658401) {
            p = 3;
        }
        return p;
    }
    static double N3246d79e208(Object []i) {
        double p = Double.NaN;
        if (i[5] == null) {
            p = 0;
        } else if (((Double) i[5]).doubleValue() <= 0.870574) {
            p = 0;
        } else if (((Double) i[5]).doubleValue() > 0.870574) {
            p = WekaClassifier.N48f5dfdb209(i);
        }
        return p;
    }
    static double N48f5dfdb209(Object []i) {
        double p = Double.NaN;
        if (i[1] == null) {
            p = 0;
        } else if (((Double) i[1]).doubleValue() <= 0.443477) {
            p = WekaClassifier.N63bc849210(i);
        } else if (((Double) i[1]).doubleValue() > 0.443477) {
            p = 3;
        }
        return p;
    }
    static double N63bc849210(Object []i) {
        double p = Double.NaN;
        if (i[5] == null) {
            p = 0;
        } else if (((Double) i[5]).doubleValue() <= 1.972881) {
            p = 0;
        } else if (((Double) i[5]).doubleValue() > 1.972881) {
            p = 3;
        }
        return p;
    }
    static double N3eda33d1211(Object []i) {
        double p = Double.NaN;
        if (i[21] == null) {
            p = 0;
        } else if (((Double) i[21]).doubleValue() <= 2.015243) {
            p = WekaClassifier.N677ffe93212(i);
        } else if (((Double) i[21]).doubleValue() > 2.015243) {
            p = WekaClassifier.N1d33f970272(i);
        }
        return p;
    }
    static double N677ffe93212(Object []i) {
        double p = Double.NaN;
        if (i[11] == null) {
            p = 0;
        } else if (((Double) i[11]).doubleValue() <= 2.860106) {
            p = WekaClassifier.N42299fb213(i);
        } else if (((Double) i[11]).doubleValue() > 2.860106) {
            p = WekaClassifier.N78323951263(i);
        }
        return p;
    }
    static double N42299fb213(Object []i) {
        double p = Double.NaN;
        if (i[64] == null) {
            p = 0;
        } else if (((Double) i[64]).doubleValue() <= 1.825757) {
            p = WekaClassifier.N7bab2fdb214(i);
        } else if (((Double) i[64]).doubleValue() > 1.825757) {
            p = WekaClassifier.N515c2a53245(i);
        }
        return p;
    }
    static double N7bab2fdb214(Object []i) {
        double p = Double.NaN;
        if (i[24] == null) {
            p = 0;
        } else if (((Double) i[24]).doubleValue() <= 1.097238) {
            p = WekaClassifier.N23d3124b215(i);
        } else if (((Double) i[24]).doubleValue() > 1.097238) {
            p = WekaClassifier.N3bfebdc2244(i);
        }
        return p;
    }
    static double N23d3124b215(Object []i) {
        double p = Double.NaN;
        if (i[4] == null) {
            p = 0;
        } else if (((Double) i[4]).doubleValue() <= 5.731045) {
            p = WekaClassifier.N76560c4c216(i);
        } else if (((Double) i[4]).doubleValue() > 5.731045) {
            p = WekaClassifier.N17876ce7243(i);
        }
        return p;
    }
    static double N76560c4c216(Object []i) {
        double p = Double.NaN;
        if (i[0] == null) {
            p = 0;
        } else if (((Double) i[0]).doubleValue() <= 29.659116) {
            p = WekaClassifier.Nb418e46217(i);
        } else if (((Double) i[0]).doubleValue() > 29.659116) {
            p = WekaClassifier.N22811ae4231(i);
        }
        return p;
    }
    static double Nb418e46217(Object []i) {
        double p = Double.NaN;
        if (i[20] == null) {
            p = 0;
        } else if (((Double) i[20]).doubleValue() <= 0.716736) {
            p = WekaClassifier.N7ce39b6f218(i);
        } else if (((Double) i[20]).doubleValue() > 0.716736) {
            p = WekaClassifier.N224e41b2227(i);
        }
        return p;
    }
    static double N7ce39b6f218(Object []i) {
        double p = Double.NaN;
        if (i[25] == null) {
            p = 1;
        } else if (((Double) i[25]).doubleValue() <= 0.048916) {
            p = 1;
        } else if (((Double) i[25]).doubleValue() > 0.048916) {
            p = WekaClassifier.N42f1d477219(i);
        }
        return p;
    }
    static double N42f1d477219(Object []i) {
        double p = Double.NaN;
        if (i[6] == null) {
            p = 0;
        } else if (((Double) i[6]).doubleValue() <= 1.023974) {
            p = WekaClassifier.N12b3f2f7220(i);
        } else if (((Double) i[6]).doubleValue() > 1.023974) {
            p = WekaClassifier.N48206eda224(i);
        }
        return p;
    }
    static double N12b3f2f7220(Object []i) {
        double p = Double.NaN;
        if (i[30] == null) {
            p = 3;
        } else if (((Double) i[30]).doubleValue() <= 0.121901) {
            p = 3;
        } else if (((Double) i[30]).doubleValue() > 0.121901) {
            p = WekaClassifier.N64143fb8221(i);
        }
        return p;
    }
    static double N64143fb8221(Object []i) {
        double p = Double.NaN;
        if (i[0] == null) {
            p = 0;
        } else if (((Double) i[0]).doubleValue() <= 21.504816) {
            p = WekaClassifier.N6d637c5c222(i);
        } else if (((Double) i[0]).doubleValue() > 21.504816) {
            p = WekaClassifier.N1e79b41f223(i);
        }
        return p;
    }
    static double N6d637c5c222(Object []i) {
        double p = Double.NaN;
        if (i[20] == null) {
            p = 0;
        } else if (((Double) i[20]).doubleValue() <= 0.577014) {
            p = 0;
        } else if (((Double) i[20]).doubleValue() > 0.577014) {
            p = 3;
        }
        return p;
    }
    static double N1e79b41f223(Object []i) {
        double p = Double.NaN;
        if (i[3] == null) {
            p = 3;
        } else if (((Double) i[3]).doubleValue() <= 2.380695) {
            p = 3;
        } else if (((Double) i[3]).doubleValue() > 2.380695) {
            p = 0;
        }
        return p;
    }
    static double N48206eda224(Object []i) {
        double p = Double.NaN;
        if (i[13] == null) {
            p = 3;
        } else if (((Double) i[13]).doubleValue() <= 0.098641) {
            p = 3;
        } else if (((Double) i[13]).doubleValue() > 0.098641) {
            p = WekaClassifier.N494dcf34225(i);
        }
        return p;
    }
    static double N494dcf34225(Object []i) {
        double p = Double.NaN;
        if (i[30] == null) {
            p = 0;
        } else if (((Double) i[30]).doubleValue() <= 0.549571) {
            p = WekaClassifier.N135e8485226(i);
        } else if (((Double) i[30]).doubleValue() > 0.549571) {
            p = 1;
        }
        return p;
    }
    static double N135e8485226(Object []i) {
        double p = Double.NaN;
        if (i[9] == null) {
            p = 1;
        } else if (((Double) i[9]).doubleValue() <= 0.297851) {
            p = 1;
        } else if (((Double) i[9]).doubleValue() > 0.297851) {
            p = 0;
        }
        return p;
    }
    static double N224e41b2227(Object []i) {
        double p = Double.NaN;
        if (i[31] == null) {
            p = 3;
        } else if (((Double) i[31]).doubleValue() <= 0.55869) {
            p = WekaClassifier.N3fbb1eb6228(i);
        } else if (((Double) i[31]).doubleValue() > 0.55869) {
            p = WekaClassifier.N78a3335c230(i);
        }
        return p;
    }
    static double N3fbb1eb6228(Object []i) {
        double p = Double.NaN;
        if (i[16] == null) {
            p = 3;
        } else if (((Double) i[16]).doubleValue() <= 0.784325) {
            p = 3;
        } else if (((Double) i[16]).doubleValue() > 0.784325) {
            p = WekaClassifier.N15dd5f6a229(i);
        }
        return p;
    }
    static double N15dd5f6a229(Object []i) {
        double p = Double.NaN;
        if (i[10] == null) {
            p = 0;
        } else if (((Double) i[10]).doubleValue() <= 0.671754) {
            p = 0;
        } else if (((Double) i[10]).doubleValue() > 0.671754) {
            p = 3;
        }
        return p;
    }
    static double N78a3335c230(Object []i) {
        double p = Double.NaN;
        if (i[3] == null) {
            p = 1;
        } else if (((Double) i[3]).doubleValue() <= 2.119684) {
            p = 1;
        } else if (((Double) i[3]).doubleValue() > 2.119684) {
            p = 0;
        }
        return p;
    }
    static double N22811ae4231(Object []i) {
        double p = Double.NaN;
        if (i[31] == null) {
            p = 1;
        } else if (((Double) i[31]).doubleValue() <= 0.114995) {
            p = WekaClassifier.N4a0c806e232(i);
        } else if (((Double) i[31]).doubleValue() > 0.114995) {
            p = WekaClassifier.N7ac3dbbc233(i);
        }
        return p;
    }
    static double N4a0c806e232(Object []i) {
        double p = Double.NaN;
        if (i[3] == null) {
            p = 3;
        } else if (((Double) i[3]).doubleValue() <= 3.109905) {
            p = 3;
        } else if (((Double) i[3]).doubleValue() > 3.109905) {
            p = 1;
        }
        return p;
    }
    static double N7ac3dbbc233(Object []i) {
        double p = Double.NaN;
        if (i[24] == null) {
            p = 3;
        } else if (((Double) i[24]).doubleValue() <= 0.31483) {
            p = WekaClassifier.N508f529b234(i);
        } else if (((Double) i[24]).doubleValue() > 0.31483) {
            p = WekaClassifier.N17cea0f5236(i);
        }
        return p;
    }
    static double N508f529b234(Object []i) {
        double p = Double.NaN;
        if (i[9] == null) {
            p = 3;
        } else if (((Double) i[9]).doubleValue() <= 1.807856) {
            p = 3;
        } else if (((Double) i[9]).doubleValue() > 1.807856) {
            p = WekaClassifier.N7178676e235(i);
        }
        return p;
    }
    static double N7178676e235(Object []i) {
        double p = Double.NaN;
        if (i[1] == null) {
            p = 3;
        } else if (((Double) i[1]).doubleValue() <= 5.209057) {
            p = 3;
        } else if (((Double) i[1]).doubleValue() > 5.209057) {
            p = 0;
        }
        return p;
    }
    static double N17cea0f5236(Object []i) {
        double p = Double.NaN;
        if (i[7] == null) {
            p = 3;
        } else if (((Double) i[7]).doubleValue() <= 0.805842) {
            p = 3;
        } else if (((Double) i[7]).doubleValue() > 0.805842) {
            p = WekaClassifier.N7ea94108237(i);
        }
        return p;
    }
    static double N7ea94108237(Object []i) {
        double p = Double.NaN;
        if (i[5] == null) {
            p = 3;
        } else if (((Double) i[5]).doubleValue() <= 2.190614) {
            p = WekaClassifier.N19ecb52f238(i);
        } else if (((Double) i[5]).doubleValue() > 2.190614) {
            p = WekaClassifier.N71963193240(i);
        }
        return p;
    }
    static double N19ecb52f238(Object []i) {
        double p = Double.NaN;
        if (i[0] == null) {
            p = 0;
        } else if (((Double) i[0]).doubleValue() <= 32.71815) {
            p = 0;
        } else if (((Double) i[0]).doubleValue() > 32.71815) {
            p = WekaClassifier.N36b2df5239(i);
        }
        return p;
    }
    static double N36b2df5239(Object []i) {
        double p = Double.NaN;
        if (i[17] == null) {
            p = 0;
        } else if (((Double) i[17]).doubleValue() <= 0.082219) {
            p = 0;
        } else if (((Double) i[17]).doubleValue() > 0.082219) {
            p = 3;
        }
        return p;
    }
    static double N71963193240(Object []i) {
        double p = Double.NaN;
        if (i[1] == null) {
            p = 3;
        } else if (((Double) i[1]).doubleValue() <= 7.473073) {
            p = WekaClassifier.N3b90e427241(i);
        } else if (((Double) i[1]).doubleValue() > 7.473073) {
            p = 0;
        }
        return p;
    }
    static double N3b90e427241(Object []i) {
        double p = Double.NaN;
        if (i[1] == null) {
            p = 0;
        } else if (((Double) i[1]).doubleValue() <= 4.158545) {
            p = WekaClassifier.N2572dafe242(i);
        } else if (((Double) i[1]).doubleValue() > 4.158545) {
            p = 3;
        }
        return p;
    }
    static double N2572dafe242(Object []i) {
        double p = Double.NaN;
        if (i[1] == null) {
            p = 3;
        } else if (((Double) i[1]).doubleValue() <= 1.78951) {
            p = 3;
        } else if (((Double) i[1]).doubleValue() > 1.78951) {
            p = 0;
        }
        return p;
    }
    static double N17876ce7243(Object []i) {
        double p = Double.NaN;
        if (i[10] == null) {
            p = 0;
        } else if (((Double) i[10]).doubleValue() <= 1.24049) {
            p = 0;
        } else if (((Double) i[10]).doubleValue() > 1.24049) {
            p = 1;
        }
        return p;
    }
    static double N3bfebdc2244(Object []i) {
        double p = Double.NaN;
        if (i[4] == null) {
            p = 0;
        } else if (((Double) i[4]).doubleValue() <= 1.946415) {
            p = 0;
        } else if (((Double) i[4]).doubleValue() > 1.946415) {
            p = 1;
        }
        return p;
    }
    static double N515c2a53245(Object []i) {
        double p = Double.NaN;
        if (i[9] == null) {
            p = 0;
        } else if (((Double) i[9]).doubleValue() <= 7.289458) {
            p = WekaClassifier.N79ded8df246(i);
        } else if (((Double) i[9]).doubleValue() > 7.289458) {
            p = 3;
        }
        return p;
    }
    static double N79ded8df246(Object []i) {
        double p = Double.NaN;
        if (i[6] == null) {
            p = 0;
        } else if (((Double) i[6]).doubleValue() <= 2.502614) {
            p = WekaClassifier.N157056fb247(i);
        } else if (((Double) i[6]).doubleValue() > 2.502614) {
            p = WekaClassifier.N7cf1d02b254(i);
        }
        return p;
    }
    static double N157056fb247(Object []i) {
        double p = Double.NaN;
        if (i[5] == null) {
            p = 0;
        } else if (((Double) i[5]).doubleValue() <= 6.733017) {
            p = WekaClassifier.N25e83bc248(i);
        } else if (((Double) i[5]).doubleValue() > 6.733017) {
            p = WekaClassifier.N6c696b07253(i);
        }
        return p;
    }
    static double N25e83bc248(Object []i) {
        double p = Double.NaN;
        if (i[27] == null) {
            p = 1;
        } else if (((Double) i[27]).doubleValue() <= 0.170756) {
            p = 1;
        } else if (((Double) i[27]).doubleValue() > 0.170756) {
            p = WekaClassifier.Nb1aacdb249(i);
        }
        return p;
    }
    static double Nb1aacdb249(Object []i) {
        double p = Double.NaN;
        if (i[21] == null) {
            p = 0;
        } else if (((Double) i[21]).doubleValue() <= 0.629366) {
            p = WekaClassifier.N44a638f250(i);
        } else if (((Double) i[21]).doubleValue() > 0.629366) {
            p = WekaClassifier.N2fce437c251(i);
        }
        return p;
    }
    static double N44a638f250(Object []i) {
        double p = Double.NaN;
        if (i[32] == null) {
            p = 0;
        } else if (((Double) i[32]).doubleValue() <= 0.601839) {
            p = 0;
        } else if (((Double) i[32]).doubleValue() > 0.601839) {
            p = 2;
        }
        return p;
    }
    static double N2fce437c251(Object []i) {
        double p = Double.NaN;
        if (i[4] == null) {
            p = 3;
        } else if (((Double) i[4]).doubleValue() <= 3.3229) {
            p = 3;
        } else if (((Double) i[4]).doubleValue() > 3.3229) {
            p = WekaClassifier.Neb09a69252(i);
        }
        return p;
    }
    static double Neb09a69252(Object []i) {
        double p = Double.NaN;
        if (i[6] == null) {
            p = 0;
        } else if (((Double) i[6]).doubleValue() <= 2.305102) {
            p = 0;
        } else if (((Double) i[6]).doubleValue() > 2.305102) {
            p = 3;
        }
        return p;
    }
    static double N6c696b07253(Object []i) {
        double p = Double.NaN;
        if (i[0] == null) {
            p = 1;
        } else if (((Double) i[0]).doubleValue() <= 82.374742) {
            p = 1;
        } else if (((Double) i[0]).doubleValue() > 82.374742) {
            p = 3;
        }
        return p;
    }
    static double N7cf1d02b254(Object []i) {
        double p = Double.NaN;
        if (i[20] == null) {
            p = 1;
        } else if (((Double) i[20]).doubleValue() <= 0.371502) {
            p = 1;
        } else if (((Double) i[20]).doubleValue() > 0.371502) {
            p = WekaClassifier.N6799f722255(i);
        }
        return p;
    }
    static double N6799f722255(Object []i) {
        double p = Double.NaN;
        if (i[1] == null) {
            p = 0;
        } else if (((Double) i[1]).doubleValue() <= 7.931389) {
            p = WekaClassifier.N2d300e51256(i);
        } else if (((Double) i[1]).doubleValue() > 7.931389) {
            p = WekaClassifier.N5566711f259(i);
        }
        return p;
    }
    static double N2d300e51256(Object []i) {
        double p = Double.NaN;
        if (i[0] == null) {
            p = 0;
        } else if (((Double) i[0]).doubleValue() <= 50.308496) {
            p = 0;
        } else if (((Double) i[0]).doubleValue() > 50.308496) {
            p = WekaClassifier.N2dfbff04257(i);
        }
        return p;
    }
    static double N2dfbff04257(Object []i) {
        double p = Double.NaN;
        if (i[13] == null) {
            p = 0;
        } else if (((Double) i[13]).doubleValue() <= 1.517451) {
            p = WekaClassifier.N7b237731258(i);
        } else if (((Double) i[13]).doubleValue() > 1.517451) {
            p = 3;
        }
        return p;
    }
    static double N7b237731258(Object []i) {
        double p = Double.NaN;
        if (i[11] == null) {
            p = 0;
        } else if (((Double) i[11]).doubleValue() <= 1.353321) {
            p = 0;
        } else if (((Double) i[11]).doubleValue() > 1.353321) {
            p = 1;
        }
        return p;
    }
    static double N5566711f259(Object []i) {
        double p = Double.NaN;
        if (i[2] == null) {
            p = 0;
        } else if (((Double) i[2]).doubleValue() <= 5.764446) {
            p = WekaClassifier.N388cd606260(i);
        } else if (((Double) i[2]).doubleValue() > 5.764446) {
            p = WekaClassifier.N263740eb261(i);
        }
        return p;
    }
    static double N388cd606260(Object []i) {
        double p = Double.NaN;
        if (i[23] == null) {
            p = 0;
        } else if (((Double) i[23]).doubleValue() <= 0.910594) {
            p = 0;
        } else if (((Double) i[23]).doubleValue() > 0.910594) {
            p = 1;
        }
        return p;
    }
    static double N263740eb261(Object []i) {
        double p = Double.NaN;
        if (i[8] == null) {
            p = 0;
        } else if (((Double) i[8]).doubleValue() <= 5.673731) {
            p = 0;
        } else if (((Double) i[8]).doubleValue() > 5.673731) {
            p = WekaClassifier.N758717e6262(i);
        }
        return p;
    }
    static double N758717e6262(Object []i) {
        double p = Double.NaN;
        if (i[8] == null) {
            p = 1;
        } else if (((Double) i[8]).doubleValue() <= 6.284908) {
            p = 1;
        } else if (((Double) i[8]).doubleValue() > 6.284908) {
            p = 0;
        }
        return p;
    }
    static double N78323951263(Object []i) {
        double p = Double.NaN;
        if (i[26] == null) {
            p = 3;
        } else if (((Double) i[26]).doubleValue() <= 0.80114) {
            p = WekaClassifier.N2951327d264(i);
        } else if (((Double) i[26]).doubleValue() > 0.80114) {
            p = WekaClassifier.N2a52f7b4267(i);
        }
        return p;
    }
    static double N2951327d264(Object []i) {
        double p = Double.NaN;
        if (i[3] == null) {
            p = 3;
        } else if (((Double) i[3]).doubleValue() <= 16.04159) {
            p = WekaClassifier.N11c9c1bc265(i);
        } else if (((Double) i[3]).doubleValue() > 16.04159) {
            p = 0;
        }
        return p;
    }
    static double N11c9c1bc265(Object []i) {
        double p = Double.NaN;
        if (i[31] == null) {
            p = 3;
        } else if (((Double) i[31]).doubleValue() <= 0.715868) {
            p = 3;
        } else if (((Double) i[31]).doubleValue() > 0.715868) {
            p = WekaClassifier.N54ce26c3266(i);
        }
        return p;
    }
    static double N54ce26c3266(Object []i) {
        double p = Double.NaN;
        if (i[2] == null) {
            p = 0;
        } else if (((Double) i[2]).doubleValue() <= 8.078847) {
            p = 0;
        } else if (((Double) i[2]).doubleValue() > 8.078847) {
            p = 3;
        }
        return p;
    }
    static double N2a52f7b4267(Object []i) {
        double p = Double.NaN;
        if (i[9] == null) {
            p = 0;
        } else if (((Double) i[9]).doubleValue() <= 6.050927) {
            p = WekaClassifier.N2d046021268(i);
        } else if (((Double) i[9]).doubleValue() > 6.050927) {
            p = WekaClassifier.N7a432d9d269(i);
        }
        return p;
    }
    static double N2d046021268(Object []i) {
        double p = Double.NaN;
        if (i[16] == null) {
            p = 0;
        } else if (((Double) i[16]).doubleValue() <= 3.273336) {
            p = 0;
        } else if (((Double) i[16]).doubleValue() > 3.273336) {
            p = 3;
        }
        return p;
    }
    static double N7a432d9d269(Object []i) {
        double p = Double.NaN;
        if (i[12] == null) {
            p = 3;
        } else if (((Double) i[12]).doubleValue() <= 3.874897) {
            p = WekaClassifier.N5067dd20270(i);
        } else if (((Double) i[12]).doubleValue() > 3.874897) {
            p = WekaClassifier.N52e6891d271(i);
        }
        return p;
    }
    static double N5067dd20270(Object []i) {
        double p = Double.NaN;
        if (i[13] == null) {
            p = 3;
        } else if (((Double) i[13]).doubleValue() <= 2.836677) {
            p = 3;
        } else if (((Double) i[13]).doubleValue() > 2.836677) {
            p = 1;
        }
        return p;
    }
    static double N52e6891d271(Object []i) {
        double p = Double.NaN;
        if (i[7] == null) {
            p = 1;
        } else if (((Double) i[7]).doubleValue() <= 3.664489) {
            p = 1;
        } else if (((Double) i[7]).doubleValue() > 3.664489) {
            p = 0;
        }
        return p;
    }
    static double N1d33f970272(Object []i) {
        double p = Double.NaN;
        if (i[29] == null) {
            p = 2;
        } else if (((Double) i[29]).doubleValue() <= 0.412275) {
            p = 2;
        } else if (((Double) i[29]).doubleValue() > 0.412275) {
            p = WekaClassifier.N3f3d370a273(i);
        }
        return p;
    }
    static double N3f3d370a273(Object []i) {
        double p = Double.NaN;
        if (i[17] == null) {
            p = 0;
        } else if (((Double) i[17]).doubleValue() <= 4.798073) {
            p = 0;
        } else if (((Double) i[17]).doubleValue() > 4.798073) {
            p = WekaClassifier.N4be891f5274(i);
        }
        return p;
    }
    static double N4be891f5274(Object []i) {
        double p = Double.NaN;
        if (i[2] == null) {
            p = 1;
        } else if (((Double) i[2]).doubleValue() <= 10.575612) {
            p = 1;
        } else if (((Double) i[2]).doubleValue() > 10.575612) {
            p = 0;
        }
        return p;
    }
    static double N11c692c2275(Object []i) {
        double p = Double.NaN;
        if (i[64] == null) {
            p = 0;
        } else if (((Double) i[64]).doubleValue() <= 3.275346) {
            p = WekaClassifier.N3cd07ac276(i);
        } else if (((Double) i[64]).doubleValue() > 3.275346) {
            p = WekaClassifier.N9b2af27277(i);
        }
        return p;
    }
    static double N3cd07ac276(Object []i) {
        double p = Double.NaN;
        if (i[20] == null) {
            p = 0;
        } else if (((Double) i[20]).doubleValue() <= 1.117372) {
            p = 0;
        } else if (((Double) i[20]).doubleValue() > 1.117372) {
            p = 1;
        }
        return p;
    }
    static double N9b2af27277(Object []i) {
        double p = Double.NaN;
        if (i[10] == null) {
            p = 1;
        } else if (((Double) i[10]).doubleValue() <= 1.076758) {
            p = WekaClassifier.N32092e6a278(i);
        } else if (((Double) i[10]).doubleValue() > 1.076758) {
            p = WekaClassifier.N78c64acf279(i);
        }
        return p;
    }
    static double N32092e6a278(Object []i) {
        double p = Double.NaN;
        if (i[4] == null) {
            p = 1;
        } else if (((Double) i[4]).doubleValue() <= 8.297866) {
            p = 1;
        } else if (((Double) i[4]).doubleValue() > 8.297866) {
            p = 2;
        }
        return p;
    }
    static double N78c64acf279(Object []i) {
        double p = Double.NaN;
        if (i[21] == null) {
            p = 0;
        } else if (((Double) i[21]).doubleValue() <= 1.456155) {
            p = WekaClassifier.N225999fb280(i);
        } else if (((Double) i[21]).doubleValue() > 1.456155) {
            p = WekaClassifier.N288c3f5b281(i);
        }
        return p;
    }
    static double N225999fb280(Object []i) {
        double p = Double.NaN;
        if (i[2] == null) {
            p = 1;
        } else if (((Double) i[2]).doubleValue() <= 3.042326) {
            p = 1;
        } else if (((Double) i[2]).doubleValue() > 3.042326) {
            p = 0;
        }
        return p;
    }
    static double N288c3f5b281(Object []i) {
        double p = Double.NaN;
        if (i[7] == null) {
            p = 1;
        } else if (((Double) i[7]).doubleValue() <= 5.446547) {
            p = WekaClassifier.Nf938429282(i);
        } else if (((Double) i[7]).doubleValue() > 5.446547) {
            p = WekaClassifier.N222336eb284(i);
        }
        return p;
    }
    static double Nf938429282(Object []i) {
        double p = Double.NaN;
        if (i[32] == null) {
            p = 0;
        } else if (((Double) i[32]).doubleValue() <= 1.614297) {
            p = WekaClassifier.N1dc9a7bc283(i);
        } else if (((Double) i[32]).doubleValue() > 1.614297) {
            p = 1;
        }
        return p;
    }
    static double N1dc9a7bc283(Object []i) {
        double p = Double.NaN;
        if (i[7] == null) {
            p = 1;
        } else if (((Double) i[7]).doubleValue() <= 1.991797) {
            p = 1;
        } else if (((Double) i[7]).doubleValue() > 1.991797) {
            p = 0;
        }
        return p;
    }
    static double N222336eb284(Object []i) {
        double p = Double.NaN;
        if (i[24] == null) {
            p = 0;
        } else if (((Double) i[24]).doubleValue() <= 3.868351) {
            p = WekaClassifier.N35ea8fcf285(i);
        } else if (((Double) i[24]).doubleValue() > 3.868351) {
            p = 0;
        }
        return p;
    }
    static double N35ea8fcf285(Object []i) {
        double p = Double.NaN;
        if (i[25] == null) {
            p = 0;
        } else if (((Double) i[25]).doubleValue() <= 1.343418) {
            p = WekaClassifier.N3a877ab0286(i);
        } else if (((Double) i[25]).doubleValue() > 1.343418) {
            p = WekaClassifier.N6229447a288(i);
        }
        return p;
    }
    static double N3a877ab0286(Object []i) {
        double p = Double.NaN;
        if (i[32] == null) {
            p = 0;
        } else if (((Double) i[32]).doubleValue() <= 0.640717) {
            p = 0;
        } else if (((Double) i[32]).doubleValue() > 0.640717) {
            p = WekaClassifier.N1487d6d5287(i);
        }
        return p;
    }
    static double N1487d6d5287(Object []i) {
        double p = Double.NaN;
        if (i[8] == null) {
            p = 0;
        } else if (((Double) i[8]).doubleValue() <= 5.696344) {
            p = 0;
        } else if (((Double) i[8]).doubleValue() > 5.696344) {
            p = 3;
        }
        return p;
    }
    static double N6229447a288(Object []i) {
        double p = Double.NaN;
        if (i[25] == null) {
            p = 1;
        } else if (((Double) i[25]).doubleValue() <= 1.392512) {
            p = 1;
        } else if (((Double) i[25]).doubleValue() > 1.392512) {
            p = WekaClassifier.N34ed7ef289(i);
        }
        return p;
    }
    static double N34ed7ef289(Object []i) {
        double p = Double.NaN;
        if (i[18] == null) {
            p = 0;
        } else if (((Double) i[18]).doubleValue() <= 2.660814) {
            p = 0;
        } else if (((Double) i[18]).doubleValue() > 2.660814) {
            p = WekaClassifier.N2d428d9b290(i);
        }
        return p;
    }
    static double N2d428d9b290(Object []i) {
        double p = Double.NaN;
        if (i[7] == null) {
            p = 1;
        } else if (((Double) i[7]).doubleValue() <= 19.917692) {
            p = WekaClassifier.N6c5ed253291(i);
        } else if (((Double) i[7]).doubleValue() > 19.917692) {
            p = 0;
        }
        return p;
    }
    static double N6c5ed253291(Object []i) {
        double p = Double.NaN;
        if (i[10] == null) {
            p = 0;
        } else if (((Double) i[10]).doubleValue() <= 3.359948) {
            p = 0;
        } else if (((Double) i[10]).doubleValue() > 3.359948) {
            p = WekaClassifier.N45427eba292(i);
        }
        return p;
    }
    static double N45427eba292(Object []i) {
        double p = Double.NaN;
        if (i[2] == null) {
            p = 1;
        } else if (((Double) i[2]).doubleValue() <= 19.52087) {
            p = 1;
        } else if (((Double) i[2]).doubleValue() > 19.52087) {
            p = WekaClassifier.N108e08dc293(i);
        }
        return p;
    }
    static double N108e08dc293(Object []i) {
        double p = Double.NaN;
        if (i[9] == null) {
            p = 1;
        } else if (((Double) i[9]).doubleValue() <= 7.503696) {
            p = 1;
        } else if (((Double) i[9]).doubleValue() > 7.503696) {
            p = 3;
        }
        return p;
    }
    static double N5ce7ac01294(Object []i) {
        double p = Double.NaN;
        if (i[0] == null) {
            p = 1;
        } else if (((Double) i[0]).doubleValue() <= 612.531119) {
            p = WekaClassifier.N6dcda54d295(i);
        } else if (((Double) i[0]).doubleValue() > 612.531119) {
            p = 2;
        }
        return p;
    }
    static double N6dcda54d295(Object []i) {
        double p = Double.NaN;
        if (i[0] == null) {
            p = 1;
        } else if (((Double) i[0]).doubleValue() <= 247.705433) {
            p = WekaClassifier.N581f9a8c296(i);
        } else if (((Double) i[0]).doubleValue() > 247.705433) {
            p = 1;
        }
        return p;
    }
    static double N581f9a8c296(Object []i) {
        double p = Double.NaN;
        if (i[15] == null) {
            p = 1;
        } else if (((Double) i[15]).doubleValue() <= 9.267788) {
            p = WekaClassifier.N2db8a87297(i);
        } else if (((Double) i[15]).doubleValue() > 9.267788) {
            p = 0;
        }
        return p;
    }
    static double N2db8a87297(Object []i) {
        double p = Double.NaN;
        if (i[15] == null) {
            p = 1;
        } else if (((Double) i[15]).doubleValue() <= 1.874072) {
            p = WekaClassifier.N1b63a688298(i);
        } else if (((Double) i[15]).doubleValue() > 1.874072) {
            p = WekaClassifier.N44df9749302(i);
        }
        return p;
    }
    static double N1b63a688298(Object []i) {
        double p = Double.NaN;
        if (i[2] == null) {
            p = 0;
        } else if (((Double) i[2]).doubleValue() <= 7.588389) {
            p = WekaClassifier.N2b4a38c4299(i);
        } else if (((Double) i[2]).doubleValue() > 7.588389) {
            p = WekaClassifier.N15d4e210300(i);
        }
        return p;
    }
    static double N2b4a38c4299(Object []i) {
        double p = Double.NaN;
        if (i[3] == null) {
            p = 0;
        } else if (((Double) i[3]).doubleValue() <= 10.68591) {
            p = 0;
        } else if (((Double) i[3]).doubleValue() > 10.68591) {
            p = 1;
        }
        return p;
    }
    static double N15d4e210300(Object []i) {
        double p = Double.NaN;
        if (i[16] == null) {
            p = 1;
        } else if (((Double) i[16]).doubleValue() <= 0.556079) {
            p = WekaClassifier.N4b4593a2301(i);
        } else if (((Double) i[16]).doubleValue() > 0.556079) {
            p = 1;
        }
        return p;
    }
    static double N4b4593a2301(Object []i) {
        double p = Double.NaN;
        if (i[3] == null) {
            p = 1;
        } else if (((Double) i[3]).doubleValue() <= 14.411571) {
            p = 1;
        } else if (((Double) i[3]).doubleValue() > 14.411571) {
            p = 2;
        }
        return p;
    }
    static double N44df9749302(Object []i) {
        double p = Double.NaN;
        if (i[8] == null) {
            p = 0;
        } else if (((Double) i[8]).doubleValue() <= 2.693318) {
            p = 0;
        } else if (((Double) i[8]).doubleValue() > 2.693318) {
            p = WekaClassifier.N33455cf2303(i);
        }
        return p;
    }
    static double N33455cf2303(Object []i) {
        double p = Double.NaN;
        if (i[26] == null) {
            p = 1;
        } else if (((Double) i[26]).doubleValue() <= 1.57314) {
            p = WekaClassifier.Ned92e2a304(i);
        } else if (((Double) i[26]).doubleValue() > 1.57314) {
            p = WekaClassifier.N503c9c47309(i);
        }
        return p;
    }
    static double Ned92e2a304(Object []i) {
        double p = Double.NaN;
        if (i[5] == null) {
            p = 1;
        } else if (((Double) i[5]).doubleValue() <= 27.364779) {
            p = WekaClassifier.N5465cf03305(i);
        } else if (((Double) i[5]).doubleValue() > 27.364779) {
            p = 0;
        }
        return p;
    }
    static double N5465cf03305(Object []i) {
        double p = Double.NaN;
        if (i[2] == null) {
            p = 0;
        } else if (((Double) i[2]).doubleValue() <= 17.393565) {
            p = WekaClassifier.N67f9f93e306(i);
        } else if (((Double) i[2]).doubleValue() > 17.393565) {
            p = WekaClassifier.N4c4cf453308(i);
        }
        return p;
    }
    static double N67f9f93e306(Object []i) {
        double p = Double.NaN;
        if (i[27] == null) {
            p = 0;
        } else if (((Double) i[27]).doubleValue() <= 0.810114) {
            p = 0;
        } else if (((Double) i[27]).doubleValue() > 0.810114) {
            p = WekaClassifier.N4c5a8ac6307(i);
        }
        return p;
    }
    static double N4c5a8ac6307(Object []i) {
        double p = Double.NaN;
        if (i[0] == null) {
            p = 0;
        } else if (((Double) i[0]).doubleValue() <= 184.394072) {
            p = 0;
        } else if (((Double) i[0]).doubleValue() > 184.394072) {
            p = 1;
        }
        return p;
    }
    static double N4c4cf453308(Object []i) {
        double p = Double.NaN;
        if (i[3] == null) {
            p = 0;
        } else if (((Double) i[3]).doubleValue() <= 10.19644) {
            p = 0;
        } else if (((Double) i[3]).doubleValue() > 10.19644) {
            p = 1;
        }
        return p;
    }
    static double N503c9c47309(Object []i) {
        double p = Double.NaN;
        if (i[0] == null) {
            p = 1;
        } else if (((Double) i[0]).doubleValue() <= 245.016145) {
            p = 1;
        } else if (((Double) i[0]).doubleValue() > 245.016145) {
            p = 0;
        }
        return p;
    }
}
